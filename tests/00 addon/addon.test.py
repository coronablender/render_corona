import unittest
import bpy
import os
import render_corona
from render_corona.tests import *
from render_corona.util    import realpath, sep

class TestAddon(unittest.TestCase):
    def test_addon_enabled(self):
        # test if addon got loaded correctly
        # every addon must provide the "bl_info" dict
        self.assertIsNotNone(render_corona.bl_info)

    def test_export_path_set(self):
        self.assertEqual(bpy.context.scene.corona.export_path, realpath('.tmp') + sep)

    def test_corona_path_set(self):
        self.assertEqual(bpy.context.user_preferences.addons["render_corona"].preferences.corona_path, os.environ["CORONA"])

RunTests(TestAddon)
