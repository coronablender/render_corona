
import bpy, os


rc = bpy.context.user_preferences.addons.new()
rc.module = "render_corona"
bpy.context.user_preferences.addons["render_corona"].preferences.convert_cycles = True
bpy.context.user_preferences.addons["render_corona"].preferences.corona_path = os.environ["CORONA"]

import shutil, filecmp, sys
import unittest
import render_corona
import render_corona.util
from render_corona.util    import realpath, sep
import xml.etree.cElementTree as ET
import logging

# Some defaults for testing
bpy.context.scene.corona.export_path = realpath(".tmp")  + sep
bpy.context.scene.corona.prog_max_passes = 2
bpy.context.scene.corona.vfb_type = '0'


render_corona.util.EnableDebug = False

def RunTests(clazz):
    # we have to manually invoke the test runner here, as we cannot use the CLI
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(clazz)
    result = unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)
    if not result.wasSuccessful():
        raise Exception("Tests Failed")

def CompareFiles(file1, file2):
    return filecmp.cmp(file1, file2, False)

def CompareTextFiles(file1, file2):
    return areFilesIdentical(file1, file2)

def areFilesIdentical(filename1, filename2):
    with open(filename1, "rU", encoding = "utf8") as a:
        with open(filename2, "rU", encoding = "utf8") as b:
            # Note that "all" and "izip" are lazy
            # (will stop at the first line that's not identical)
            return all(lineA == lineB for lineA, lineB in zip(a.readlines(), b.readlines()))

def CompareXml(file1, file2):
    result = False
    with open(realpath(file1), 'r') as f1:
        str1 = f1.read()
        str1 = str1.replace('\\', '/')
        tree1 = XmlTree.convert_string_to_tree(str1)
        with open(realpath(file2), 'r') as f2:
            str2 = f2.read()
            str2 = str2.replace('\\', '/')
            tree2 = XmlTree.convert_string_to_tree(str2)
            result = XmlTree().xml_compare(tree1, tree2, excludes=['name'])

    return result

def CompareImg(file1, file2, threshold):

    i1 = bpy.data.images.load(realpath(file1))
    i2 = bpy.data.images.load(realpath(file2))
    if i1.size[0] != i2.size[0] or i1.size[1] != i2.size[1]:
        print("Different sizes")
        return False

    pairs = zip(list(i1.pixels), list(i2.pixels))
    dif = sum(abs(p1-p2) for p1,p2 in pairs)

    ncomponents = i2.size[0] * i2.size[1] * 4
    print("Dif", dif, ncomponents)
    pc = (dif * 100) / ncomponents
    print("Difference (percentage):", pc)
    if pc > threshold:
        print('Images are different', file1, ' ', file2)
    return pc <= threshold

class XmlTree():

    def __init__(self):
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.hdlr = logging.StreamHandler(sys.stdout) #logging.FileHandler('xml-comparison.log')
        self.hdlr.setLevel(logging.DEBUG)
        self.formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.hdlr.setFormatter(self.formatter)
        self.logger.addHandler(self.hdlr)

    @staticmethod
    def convert_string_to_tree( xmlString):

        return ET.fromstring(xmlString)

    def xml_compare(self, x1, x2, excludes=[]):
        """
        Compares two xml etrees
        :param x1: the first tree
        :param x2: the second tree
        :param excludes: list of string of attributes to exclude from comparison
        :return:
            True if both files match
        """

        if x1.tag != x2.tag:
            self.logger.debug('Tags do not match: %s and %s' % (x1.tag, x2.tag))
            return False
        for name, value in x1.attrib.items():
            if not name in excludes:
                if x2.attrib.get(name) != value:
                    self.logger.debug('Attributes do not match: %s=%r, %s=%r'
                                 % (name, value, name, x2.attrib.get(name)))
                    return False
        for name in x2.attrib.keys():
            if not name in excludes:
                if name not in x1.attrib:
                    self.logger.debug('x2 has an attribute x1 is missing: %s'
                                 % name)
                    return False
        if not self.text_compare(x1.text, x2.text):
            self.logger.debug('text: %r != %r' % (x1.text, x2.text))
            return False
        if not self.text_compare(x1.tail, x2.tail):
            self.logger.debug('tail: %r != %r' % (x1.tail, x2.tail))
            return False
        cl1 = x1.getchildren()
        cl2 = x2.getchildren()
        if len(cl1) != len(cl2):
            self.logger.debug('children length differs, %i != %i'
                         % (len(cl1), len(cl2)))
            return False
        i = 0
        for c1, c2 in zip(cl1, cl2):
            i += 1
            if not c1.tag in excludes:
                if not self.xml_compare(c1, c2, excludes):
                    self.logger.debug('children %i do not match: %s'
                                 % (i, c1.tag))
                    return False
        return True

    def text_compare(self, t1, t2):
        """
        Compare two text strings
        :param t1: text one
        :param t2: text two
        :return:
            True if a match
        """
        if not t1 and not t2:
            return True
        if t1 == '*' or t2 == '*':
            return True
        return (t1 or '').strip() == (t2 or '').strip()