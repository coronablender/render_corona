import bpy
from ..export   import export_object_as_proxy
#---------------------------------
class CoronaAddObject( bpy.types.Operator):
    bl_label = "Add Object"
    bl_idname = "corona.add_object"

    material = bpy.props.StringProperty(name="Selected Node")
    instance_mesh = bpy.props.StringProperty(name="Selected Instance")

    def execute(self, context):
        mat = context.object.active_material
        node_tree = mat.node_tree
        node = node_tree.nodes[self.material]
        collection = node.includes
        num = collection.__len__()
        collection.add()
        collection[num].name = self.instance_mesh
        return {'FINISHED'}

    def check(self, context):
        return False

    def invoke( self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=300)

    def draw(self, context):
        layout = self.layout
        mat = context.object.active_material
        crn_mat = mat.corona
        node_tree = mat.node_tree
        node = node_tree.nodes[self.material]
        layout.prop_search( self, "instance_mesh", context.scene, "objects")

#-----------------------------------
#Operator for removing render passes
class CoronaRemoveObject( bpy.types.Operator):
    bl_label = "Remove Object"
    bl_idname = "corona.remove_object"

    material = bpy.props.StringProperty(name="Selected Node")
    index = bpy.props.IntProperty(name="Selected Index")

    def invoke( self, context, event):
        mat = context.object.active_material
        node_tree = mat.node_tree
        node = node_tree.nodes[self.material]
        collection = node.includes
        collection.remove(self.index)

        return {'FINISHED'}

class CoronaAddSelectedObject( bpy.types.Operator):
    bl_label = "Add Selected"
    bl_idname = "corona.add_selected"

    node = bpy.props.StringProperty(name="Selected Node")

    def invoke( self, context, event):
        mat = context.object.active_material
        node_tree = mat.node_tree
        node = node_tree.nodes[self.node]
        collection = node.includes
        current = [obj.name for obj in collection]
        obs =  [obj for obj in context.selected_objects if obj != context.object and obj.name not in current]
        for obj in obs:
            num = collection.__len__()
            collection.add()
            collection[num].name = obj.name
        return {'FINISHED'}


class CoronaCopyObjectName(bpy.types.Operator):
    bl_label = "Update proxy name"
    bl_idname = "corona.copy_object_name"

    def execute(self, context):
        bpy.context.object.corona.proxy_export_name = bpy.context.object.name
        return {'FINISHED'}

class CoronaCreateProxy(bpy.types.Operator):
    bl_label = "Create Proxy"
    bl_idname = "corona.create_proxy"

    def execute(self, context):
        self._inst_obs = {} #workaround, write_binary_obj requires

        #Sanitizing input
        if bpy.context.object.corona.proxy_export_path == '' and bpy.context.object.corona.proxy_export_name == '':
            self.report({'WARNING'}, 'Incorrect path and name')
            return {'FINISHED'}
        if bpy.context.object.corona.proxy_export_path == '':
            self.report({'WARNING'}, 'Incorrect path')
            return {'FINISHED'}
        if bpy.context.object.corona.proxy_export_name == '':
            self.report({'WARNING'}, 'Incorrect name')
            return {'FINISHED'}

        if export_object_as_proxy( self, bpy.context.object, bpy.context.scene, bpy.context.object.corona.proxy_export_path, bpy.context.object.corona.proxy_export_name):
            self.report({'INFO'}, 'Proxy has been created')
        return {'FINISHED'}


#===============================================================================

def register():
    bpy.utils.register_class( CoronaAddObject)
    bpy.utils.register_class( CoronaRemoveObject)
    bpy.utils.register_class( CoronaAddSelectedObject)
    bpy.utils.register_class( CoronaCopyObjectName)
    bpy.utils.register_class( CoronaCreateProxy)

def unregister():
    bpy.utils.unregister_class( CoronaAddObject)
    bpy.utils.unregister_class( CoronaRemoveObject)
    bpy.utils.unregister_class( CoronaAddSelectedObject)
    bpy.utils.unregister_class( CoronaCopyObjectName)
    bpy.utils.unregister_class( CoronaCreateProxy)
