import bpy
import traceback
import os
from bpy.props import StringProperty, BoolProperty, FloatProperty
from .util import setVerbose, setDebug, setLowPrecision, setMultiThreaded

# updater ops import, all setup in this file
from . import addon_updater_ops

def dummyupdate(self=None, context=None):
  try:
    setLowPrecision(bpy.context.user_preferences.addons[__package__].preferences.corona_low_precision)
    setDebug(bpy.context.user_preferences.addons[__package__].preferences.corona_debug)
    setVerbose(bpy.context.user_preferences.addons[__package__].preferences.corona_verbose)
    setMultiThreaded(bpy.context.user_preferences.addons[__package__].preferences.corona_multithreaded)

    from .properties.material import CoronaMatProps, dummyupdate as mtldummyupdate
    CoronaMatProps.preview_quality = FloatProperty( name = "Preview Quality",
                                  description = "Quality of material preview. Raising this value will increase render time for material preview",
                                  default = bpy.context.user_preferences.addons[__package__].preferences.corona_default_preview_quality,
                                  min = 0.0,
                                  max = 10,
                                  step = 10,
                                  precision = 1,
                                  update = mtldummyupdate)

    if bpy.context.user_preferences.addons[__package__].preferences.corona_exe:
      bpy.context.user_preferences.addons[__package__].preferences.corona_path = os.path.join(bpy.context.user_preferences.addons[__package__].preferences.corona_path, bpy.context.user_preferences.addons[__package__].preferences.corona_exe)
      bpy.context.user_preferences.addons[__package__].preferences.corona_exe = ''

  except Exception as e:
    traceback.print_exc()


#------------------------------------
# User preferences UI
#------------------------------------
class CoronaPreferencesPanel( bpy.types.AddonPreferences):
    bl_idname = __package__
    corona_path = StringProperty( name = "Corona Path",
                                       description = "Path to Corona executable",
                                       subtype = 'FILE_PATH',
                                       default = "")
    corona_exe = StringProperty( name = "Corona Executable",
                                       description = "Corona executable filename",
                                       subtype = 'NONE',
                                       default = "CoronaStandalone_Release.exe")

    corona_use_mtl_preview = BoolProperty(
      name = "Use Material Preview",
      description = "Use a different executable when rendering the material preview",
      default = False
    )
    corona_mtl_preview = StringProperty(
        name = "Material Preview",
        description = "Full path to corona standalone executable to be used for material preview",
        subtype = 'FILE_PATH',
        default = ''
    )

    corona_debug = BoolProperty( name = "Corona Debug",
                                description = "Enable debug output in the system console to help with errors",
                                default = False,
                                update = dummyupdate)

    corona_low_precision = BoolProperty( name = "Corona Low Precision",
                                description = "Enable low precision transform matrix, can help speed up export for thousands of instances",
                                default = False,
                                update = dummyupdate)

    corona_verbose = BoolProperty( name = "Corona Verbose",
                                description = "Enable pretty printing xml (slower) to help with errors",
                                default = False,
                                update = dummyupdate)

    corona_multithreaded = BoolProperty( name = "Corona Multi Threaded",
                                description = "Speed up exporting by using all your CPU cores",
                                default = True,
                                update = dummyupdate)

    convert_cycles = BoolProperty( name = "Convert Cycles nodes to Corona nodes",
                                description = "Automatically try to convert Cycle node layout to a Corona node layout",
                                default = True,
                                update = dummyupdate)

    corona_default_preview_quality = FloatProperty( name = "Default Preview Quality",
                                description = "Quality of material preview for new materials created. Raising this value will increase render time for material preview",
                                default = 0.3,
                                min = 0.0,
                                max = 10,
                                step = 10,
                                precision = 1,
                                update = dummyupdate)

    # addon updater preferences

    auto_check_update = bpy.props.BoolProperty(
      name = "Auto-check for Update",
      description = "If enabled, auto-check for updates using an interval",
      default = False,
      )

    updater_intrval_months = bpy.props.IntProperty(
      name='Months',
      description = "Number of months between checking for updates",
      default=0,
      min=0
      )
    updater_intrval_days = bpy.props.IntProperty(
      name='Days',
      description = "Number of days between checking for updates",
      default=7,
      min=0,
      )
    updater_intrval_hours = bpy.props.IntProperty(
      name='Hours',
      description = "Number of hours between checking for updates",
      default=0,
      min=0,
      max=23
      )
    updater_intrval_minutes = bpy.props.IntProperty(
      name='Minutes',
      description = "Number of minutes between checking for updates",
      default=0,
      min=0,
      max=59
      )

    def draw(self, context):
      self.layout.prop( self, "corona_path")
      # self.layout.prop( self, "corona_exe")
      self.layout.prop( self, "corona_use_mtl_preview")
      row = self.layout.row()
      row.prop( self, "corona_mtl_preview")
      row.enabled = self.corona_use_mtl_preview
      self.layout.prop( self, "corona_debug")
      self.layout.prop( self, "corona_verbose")
      self.layout.prop( self, "corona_multithreaded")
      self.layout.prop( self, "convert_cycles")
      self.layout.prop( self, "corona_low_precision")
      self.layout.prop( self, "corona_default_preview_quality")

      # updater draw function
      addon_updater_ops.update_settings_ui(self,context)


class DemoUpdaterPanel(bpy.types.Panel):
  """Panel to demo popup notice and ignoring functionality"""
  bl_label = "Corona Updater"
  bl_idname = "OBJECT_PT_corona_updater"
  bl_space_type = 'VIEW_3D'
  bl_region_type = 'TOOLS'
  bl_context = "objectmode"
  bl_category = "Tools"

  def draw(self, context):
    layout = self.layout

    # Call to check for update in background
    # note: built-in checks ensure it runs at most once
    # and will run in the background thread, not blocking
    # or hanging blender
    # Internal also checks to see if auto-check enabeld
    # and if the time interval has passed
    # addon_updater_ops.check_for_update_background(context)


    # could also use your own custom drawing
    # based on shared variables
    if addon_updater_ops.updater.async_checking == True:
      layout.label("Corona Render checking for update", icon="INFO")
    elif addon_updater_ops.updater.update_ready != True:
      layout.label("Corona Render up to date", icon="INFO")

    # call built-in function with draw code/checks
    addon_updater_ops.update_notice_box_ui(self, context)



def register():

    bpy.utils.register_class( DemoUpdaterPanel)
    bpy.utils.register_class( CoronaPreferencesPanel)
    addon_updater_ops.check_for_update_background()
    dummyupdate()

def unregister():
    bpy.utils.unregister_class( DemoUpdaterPanel)
    bpy.utils.unregister_class( CoronaPreferencesPanel)
