import bpy, bpy_extras
import random, re
import multiprocessing
import os, datetime
import math, mathutils
import urllib.request
import traceback
import sys
import linecache
from collections            import defaultdict
from bpy_extras.io_utils    import axis_conversion
from shutil                 import copyfile
from math                   import tan, atan, degrees, log
from .                      import bl_info, temp_data


from xml.etree import cElementTree as ElementTree
from xml.dom import minidom

#------------------------------------
# Module settings.
#------------------------------------
Verbose = False
EnableDebug = False
MultiThreaded = True
EnableLowPrecision = False

# This is blender format to unique corona format name
MaterialDict = {}
# This is corona format to blender format
CoronaMaterialDict ={}
CoronaNameRegex = re.compile("\W")

sep = os.path.sep

def setVerbose(verbose):
    global Verbose
    Verbose = verbose

def setDebug(debug):
    global EnableDebug
    EnableDebug = debug

def setLowPrecision(lowPrecision):
    global EnableLowPrecision
    EnableLowPrecision = lowPrecision

def setMultiThreaded(multithreaded):
    global MultiThreaded
    MultiThreaded = multithreaded

def getMultiThreaded():
    global MultiThreaded
    return MultiThreaded

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    global Verbose
    debug("Verbose ", Verbose)
    rough_string = ElementTree.tostring(elem, encoding="utf-8")
    if Verbose:
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ", encoding='utf-8')
    else:
        return rough_string

def resolveMaterialName(matname):
    global MaterialDict, CoronaMaterialDict
    # Shortcut we already know the material name
    if matname in MaterialDict:
        # debug("Found material %s in MaterialDict: %s" % (matname, MaterialDict[matname]))
        return MaterialDict[matname]
    # Using corona rules look up the corona material dict
    newName = matname
    coronaName = coronaFormat(newName)
    while coronaName in CoronaMaterialDict:
        # Make sure the corona match is actually the name we want
        if CoronaMaterialDict[coronaName] == matname:
            # debug("Found material %s in CoronaMaterialDict: %s, Corona: %s" % (matname, MaterialDict[matname], coronaName))
            return MaterialDict[matname]
        debug("Making the name unique")
        # Make a new unique name
        newName = "%s_c" % newName
        coronaName = coronaFormat(newName)
        # Run the test again with the new name
    # Ok we reached here so we have a newName and a coronaName and the original matname
    debug("Registering material %s as %s, Corona: %s" % (matname, newName, coronaName))
    CoronaMaterialDict[coronaName] = matname
    MaterialDict[matname] = newName
    debug(CoronaMaterialDict)
    debug(MaterialDict)
    return newName

def resetMaterialNames():
    global MaterialDict, CoronaMaterialDict
    MaterialDict = {}
    CoronaMaterialDict = {}

def coronaFormat(matname):
    global CoronaNameRegex
    # Need to replace stuff with underscore too...
    return CoronaNameRegex.sub("_", matname).lower()

def debug( *args):
    global EnableDebug
    if EnableDebug:
        msg = ' '.join(['%s'%a for a in args])
        print( "debug:  ", msg)
    else:
        pass

def print_exception(e):
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print( 'CORONA: EXCEPTION IN ({} LINE {}): {}'.format(filename, lineno, exc_obj) )

def CrnUpdate( *args):
    msg = ' '.join(['%s'%a for a in args])
    print( "Corona:", msg)

def CrnProgress( *args):
    msg = ' '.join(['%s'%a for a in args])
    print( "Progress:", msg)

def CrnInfo( *args):
    msg = ' '.join(['%s'%a for a in args])
    print( "Info:", msg)

def CrnError( *args):
    msg = ' '.join(['%s'%a for a in args])
    print( "------")
    print( "Error:", msg)
    print( "------")


#------------------------------------
# Generic utilities and settings.
#------------------------------------
script_name = "render_corona"
bitbucket_version = None

# Addon directory.
addon_paths = bpy.utils.script_paths("addons")
if __package__ in os.listdir(addon_paths[0]):
    addon_dir = os.path.join( addon_paths[0], __package__)
else:
    addon_dir = os.path.join( addon_paths[1], __package__)

# Total system threads.
thread_count = multiprocessing.cpu_count()

sep = os.path.sep

node_colors = {
    'CoronaMtlNode': ['Diffuse Color', 'Reflect Color', 'Translucency Color'],
    'CoronaMixNode': ['Color A', 'Color B'],
    'CoronaHueNode': ['Color'],
    'CoronaToneMapNode': ['Child'],
    'CoronaRGBCurveNode': ['Input'],
    'CoronaSolidNode': ['color'],
    'CoronaRaySwitchOutputNode': ['Normal', 'Reflect', 'Refract', 'Direct'],
    'CoronaLightMtlNode': ['Emission Color'],
    'CoronaLayeredOutputNode': ['Base'],
    'CoronaSeparateRGBNode': ['Color']
}
node_textures = {
    'CoronaMtlNode': ['Diffuse Color', 'Reflect Color', 'Translucency Color'],
    'CoronaMixNode': ['Color A', 'Color B'],
    'CoronaHueNode': ['Color'],
    'CoronaToneMapNode': ['Child'],
    'CoronaRGBCurveNode': ['Input'],
    'CoronaSolidNode': [],
    'CoronaRaySwitchOutputNode': ['Normal', 'Reflect', 'Refract', 'Direct'],
    'CoronaLightMtlNode': [],
    'CoronaLayeredOutputNode': ['Base'],
    'CoronaSeparateRGBNode': ['Color']
}

def random_seed():
    return random.randrange(1, 2000)
def get_version():
    return ".".join(map(str, bl_info["version"]))

def get_version_string():
    return "version " + get_version()

def get_bitbucket_version():
    global bitbucket_version
    if bitbucket_version:
        return bitbucket_version
    try:
        bitbucket_version = urllib.request.urlopen("https://api.bitbucket.org/1.0/repositories/coronablender/render_corona/raw/master/version.txt", data=None, timeout=3).read().decode('utf-8').strip()
    except:
        bitbucket_version = get_version()

    return bitbucket_version


def strip_spaces( name):
    return ('_').join( name.split(' '))

def join_names_underscore( name1, name2):
    return ('_').join( (strip_spaces( name1), strip_spaces( name2)))

def join_params( params, directive):
    return ('').join( (('').join( params), directive))

def name_compat(name):
    if name is None:
        return 'None'
    else:
        return re.sub('[^a-zA-Z0-9_.]', '_', name)
        #name.replace(' ', '_').replace(":", "_").replace("\"", "_").replace("/", "_").replace("\\", "_").replace("*")

def same(a, b):
    if ("%s" % type(a)) == "<class 'bpy_prop_array'>":
        it = [a1 == b1 for (a1, b1) in zip(a,b)]
        result = all(it)
        if not result:
            for (a1, b1) in zip(a,b):
                if a1 != b1:
                    debug('----', a1, b1)
        return result
    return a == b

def mod_equality(mod1, mod2, ignore=["name"]):
    it = [same(getattr(mod1, prop, True), getattr(mod2, prop, False))
             for prop in mod1.bl_rna.properties.keys()
             if prop not in ignore]
    result = all(it)
    if not result:
        debug("Modifiers different:", mod1, mod2)
        for prop in mod1.bl_rna.properties.keys():
            if prop in ignore:
                continue
            if not same(getattr(mod1, prop, True), getattr(mod2, prop, False)):
                a = getattr(mod1, prop, True)
                b = getattr(mod2, prop, False)
                debug("--", prop, a, b)
    return result

def same_modifier_stack_ordered(obj1, obj2):
    if len(obj2.modifiers) != len(obj1.modifiers):
        return False
    return all([mod_equality(m, obj2.modifiers[i])
                for i,m in enumerate(obj1.modifiers)])

def get_export_name(_exported_obs, obj):
    name, needs_export = get_name_needs_export(_exported_obs, obj)
    return name

def get_name_needs_export(_exported_obs, obj):
    needs_export = True
    needs_unique_name = False
    if obj.data is None:
        return name_compat(obj.name), False
    name = obj.data.name
    # This should be the same order as export
    for exported, exported_name in _exported_obs:
        if obj.data.name == exported.data.name:
            if same_modifier_stack_ordered(obj, exported):
                name = exported_name
                needs_unique_name = False
                needs_export = False
                break
            else:
                needs_unique_name = True
                needs_export = True

    if needs_unique_name:
        name = obj.data.name + obj.name
        debug("mesh is the same but modifiers are different", name)

    name = name_compat(name)
    _exported_obs.append((obj, name))
    return name, needs_export

def extract_objs(parent, scene, target = None, context = None, gi_visibility=None):
    #gi_visibility = group instance visibility
    debug("Extract objs", parent, scene, target, context)
    if not parent:
        return
    if target is None:
        target = []
    for obj in parent.objects:
        debug('Extract objects xx:', obj, obj.type, obj.dupli_type)
        if obj.dupli_type == 'GROUP':
            extract_objs(obj.dupli_group, scene, target, obj.name, gi_visibility=is_in_export_layer(parent, scene))
        if do_export(obj, scene, context, gi_visibility):
            target.append((obj, context))

    debug("Target for context", context, target)
    return target

def filter_flatten_params( params):
    filter_list = []
    for p in params:
        if p not in filter_list:
            filter_list.append( p)
    return ('').join( filter_list)

def get_timestamp():
    now = datetime.datetime.now()
    return "%d-%d-%d %d:%d:%d\n" % (now.month, now.day, now.year, now.hour, now.minute, now.second)

def getTransform(ob_mat):
    tm = ''
    global EnableLowPrecision
    if EnableLowPrecision:
        for i in range( 0, 3):
            tm = tm + '%.4f %.4f %.4f %.4f ' % (ob_mat[i][0],
                                                ob_mat[i][1],
                                                ob_mat[i][2],
                                                ob_mat[i][3])
    else:
        for i in range( 0, 3):
            tm = tm + '%.8f %.8f %.8f %.8f ' % (ob_mat[i][0],
                                                ob_mat[i][1],
                                                ob_mat[i][2],
                                                ob_mat[i][3])

    return tm

def findVertexGroupName(face, vWeightMap):
    """
    Searches the vertexDict to see what groups is assigned to a given face.
    We use a frequency system in order to sort out the name because a given vetex can

    belong to two or more groups at the same time. To find the right name for the face
    we list all the possible vertex group names with their frequency and then sort by
    frequency in descend order. The top element is the one shared by the highest number
    of vertices is the face's group

    """
    weightDict = {}
    for vert_index in face.vertices:
        vWeights = vWeightMap[vert_index]
        for vGroupName, weight in vWeights:
            weightDict[vGroupName] = weightDict.get(vGroupName, 0.0) + weight

    if weightDict:
        return max((weight, vGroupName) for vGroupName, weight in weightDict.items())[1]
    else:
        return '(null)'


def in_scene_layer(ob, scene):
    for i in range(len(ob.layers)):
        if ob.layers[i] and scene.layers[i]:
            return True
        else:
            continue
    return False

#check if obj should be exported
def do_export(obj, scene, context = None, gi_in_export_layer = None):
    if gi_in_export_layer != None:
        in_export_layer = gi_in_export_layer
    else:
        in_export_layer = is_in_export_layer(obj, scene)

    return not obj.hide_render and obj.type in ('MESH', 'SURFACE', 'META', 'TEXT', 'CURVE', 'FONT') and in_export_layer

def is_in_export_layer(obj, scene):
    if is_local_mode():
        for i in range(len(obj.layers_local_view)):
            if obj.layers_local_view[i] == True:
                return True
    else:
        for i in range(20): #max layers?
            if obj.layers[i] and scene.layers[i] and scene.render.layers.active.layers[i]:
                return True
            else:
                continue
    return False

#check if local mode is active (3D VIEW -> NUM /)
def is_local_mode():
    local_layers = None
    for window in bpy.context.window_manager.windows:
        screen = window.screen
        for area in screen.areas:
            if area.type == 'VIEW_3D':
                space = area.spaces[0]
                local_layers = space.layers_local_view[:]
                break
    return local_layers is not None and True in local_layers

def in_local_layer(ob, layers):
    for i in range( len(ob.layers_local_view)):
        if ob.layers_local_view[i] and layers[i]:
            return True
        else:
            continue

def confpath(path):
    return '"%s"' % realpath(path).replace('\\', '/')

def realpath(path, obj=None):
    return os.path.realpath(filesystem_path(path, obj))


def filesystem_path(p, obj=None):
    """Resolve a relative Blender path to a real filesystem path"""
    if p.startswith('//'):
        if obj and hasattr(obj, 'library') and obj.library:
            pout = bpy.path.abspath(p, library = obj.library)
        else:
            pout = bpy.path.abspath(p)
    else:
        pout = os.path.realpath(p)

    return pout.replace('\\', '/')

def resolve_export_path(crn_scene):
    path = crn_scene.export_path
    if path.find('$filename') != -1:
        blendName = bpy.context.blend_data.filepath
        if blendName == '':
            if path == "//$filename":
                blendName = bpy.app.tempdir + sep + 'corona'
            else:
                raise Exception('Please save the .blend if you use $filename in the export path (//$filename will use the temp directory)')
        else:
            blendName = os.path.splitext(os.path.basename(blendName))[0]
        path = path.replace('$filename', blendName)
    path = realpath(path);
    try:
        os.makedirs(path, exist_ok = True)
    except:
        pass
    return path

def get_socket_color(socket):
    return None
    color = None
    if socket.is_linked and len(socket.links) > 0:
        node = socket.links[0].from_node
        try:
            colors = node_colors[node.bl_idname]
            for nodeName in colors:
                if node.inputs.find(nodeName) == -1:
                    color = getattr(node, nodeName, None)
                else:
                    color = get_socket_color(node.inputs[nodeName])
                    if color is not None:
                        if not node.inputs[nodeName].is_linked:
                            try:
                                level = node.inputs[nodeName.replace("Color", "Level")].default_value
                                if level == 0:
                                    color = None
                            except:
                                pass
                if color is not None:
                    break;
        except:
            debug('Info: Unknown node %s' % node.bl_idname)
    else:
        try:
            color = socket.default_value
            if isinstance(color, float):
                color = None
        except:
            color = None
    if color is not None:
        if not isinstance(color, mathutils.Color):
            color = list(color[0:3])
    return color

def get_socket_texture(socket):
    tex = None
    if socket.is_linked and len(socket.links) > 0:
        node = socket.links[0].from_node
        if node.bl_idname == 'CoronaTexNode':
            tex = node.tex_path
        else:
            try:
                textures = node_textures[node.bl_idname]
                for nodeName in textures:
                    tex = get_socket_texture(node.inputs[nodeName])
                    if tex is not None:
                        break;
            except:
                debug('Info: Unknown node %s' % node.bl_idname)
    return tex

def get_mat_tex(crn_mat):
    if crn_mat.use_map_kd and crn_mat.map_kd.texture != '' and is_uv_img( crn_mat.map_kd.texture):
        return get_tex_path(crn_mat.map_kd.texture)
    return None

def getOutputNode(material):
    node = None
    if material.use_nodes and material.corona.use_nodes and material.node_tree:
        node_tree = material.node_tree
        if node_tree.active_output != -1 and \
            node_tree.nodes[node_tree.active_output].bl_idname == "CoronaOutputNode":
            node = node_tree.nodes[node_tree.active_output]
        if not node:
            for n in node_tree.nodes:
                if n.bl_idname == "CoronaOutputNode":
                    node = n
    return node

def findCoronaOutputNode(material):
    node = None
    if material.node_tree:
        node_tree = material.node_tree
        if node_tree.active_output != -1 and \
            node_tree.nodes[node_tree.active_output].bl_idname == "CoronaOutputNode":
            node = node_tree.nodes[node_tree.active_output]
        if not node:
            for n in node_tree.nodes:
                if n.bl_idname == "CoronaOutputNode":
                    node = n
    return node

def view3D_mat_update(self, context, custom_node = None):
    mat = context.object.active_material
    if not mat:
        return
    crn_mat = mat.corona
    color = crn_mat.kd
    texture = None
    node = getOutputNode(mat)

    if node:
        color = node.get_viewport_color()

    if custom_node:
        if node:
            texture = bpy.data.textures.get(custom_node.init_texture_name)
            if texture is not None:
                if texture.image != None:
                    texture = texture.image.filepath
                else:
                    return False
        else:
            texture = get_mat_tex(crn_mat)
    else:
        if node:
            texture = get_socket_texture(node.inputs["Material"])
        else:
            texture = get_mat_tex(crn_mat)


    if texture is not None and texture != '':
        try:
            image = bpy.data.images.load(realpath(texture), check_existing=True)
            mat.use_face_texture = True

            if not mat.node_tree:
                mat.use_nodes = True

            #create cycles texture node to show image in the viewport
            texNode = mat.node_tree.nodes.get("CoronaUVMappingPreviewNode")
            if not texNode:
                texNode = mat.node_tree.nodes.new("ShaderNodeTexImage")
                texNode.name = "CoronaUVMappingPreviewNode"
                texNode.label = "Viewport Image"
                mat.node_tree.nodes.active = texNode
                texNode.select = False
                texNode.hide = True

                #hide all inputs
                for i in range(len(texNode.inputs)):
                    texNode.inputs[i].hide = True
                for i in range(len(texNode.outputs)):
                    texNode.outputs[i].hide = True

            if texNode:
                #location and size of the node based on the output node
                output_node = getOutputNode(mat)
                if output_node:
                    texNode.location = [output_node.location[0] + 0, output_node.location[1] - output_node.dimensions[1]]
                    texNode.width = output_node.width

                #show image in uv editor
                if hasattr(bpy.context.object.data.uv_textures.active, 'data'):
                    bpy.context.object.data.uv_textures.active.data[0].image = image
                for area in bpy.context.screen.areas:
                    if area.type == 'IMAGE_EDITOR':
                        area.spaces.active.image = image


                texNode.image = image
        except:
            debug("Failed to use texture")
            traceback.print_exc()
            pass
    else:
        if mat.node_tree:
            for node in mat.node_tree.nodes:
                if node.name.startswith("CoronaUVMappingPreviewNode"):
                    try:
                        mat.node_tree.nodes.remove(node)
                    except:
                        pass

    mat.use_face_texture = False
    mat.diffuse_color = color if color else [0.7, 0.7, 0.7]

def scene_enumerator(self, context):
    matches = []
    for scene in bpy.data.scenes:
        matches.append((scene.name, scene.name, ""))
    return matches

def camera_enumerator(self, context):
    return object_enumerator('CAMERA')

def object_enumerator(self, context):
    matches = []
    for object in context.scene.objects:
        if object.type in {'MESH', 'SURFACE'}:
            matches.append((object.name, object.name, ""))
    return matches

def sun_enumerator(self, context):
    sun = []
    for object in context.scene.objects:
        if object.type == 'LAMP':
            if object.data.type == 'SUN':
                sun.append((object.name, object.name, ""))
    return sun


def node_enumerator( self, context):
    nodes = []
    tree = bpy.data.node_groups[ context.object.active_material.corona.node_tree]
    for node in tree.nodes:
        if node.bl_idname in { 'CoronaMtlNode', 'CoronaLightMtlNode'}:
            nodes.append((node.name, node.name, ""))
    return nodes


def mat_enumerator(self, context):
    materials = []
    for material in bpy.data.materials:
        materials.append((material.name, material.name, ""))
    return materials

def is_uv_img( texture_name):
    texture  = bpy.data.textures.get(texture_name, None)
    if bool(texture) and hasattr(texture, "image"):
        if texture.image.file_format in {'OPEN_EXR', 'PNG', 'BMP', 'JPEG', 'TIFF', 'TARGA'}:
            if texture.image.packed_file:
                texture.image.unpack()
            return True
        else:
            CrnInfo("%s format not supported, skipping image texture %s" % ( texture.image.file_format, texture.image.name))
    else:
        return False

def get_tex_img(texture_name):
    return bpy.data.textures[texture_name].image.filepath.split(sep)[-1]

def get_tex_path(texture_name):
    return realpath(bpy.data.textures[texture_name].image.filepath)

def is_proxy(ob, scene):
    proxy = False
    if ob.type == 'MESH' and ob.corona.is_proxy:
        if ob.corona.use_external:
            proxy = ob.corona.external_instance_mesh != ''
        elif ob.corona.instance_mesh != '':
            proxy = scene.objects[ob.corona.instance_mesh].type == 'MESH'
    return proxy

def get_all_psysobs():
    obs = set()
    for settings in bpy.data.particles:
        if settings.render_type == 'OBJECT' and settings.dupli_object is not None:
            obs.add( settings.dupli_object)
        elif settings.render_type == 'GROUP' and settings.dupli_group is not None:
            obs.update( {ob for ob in settings.dupli_group.objects})
    return obs

def get_all_duplis( scene ):
    obs = set()
    for ob in scene.objects:
        if ob.parent and ob.parent.dupli_type in {'FACES', 'VERTS', 'GROUP'}:
            obs.add( ob)
    return obs

def get_matrix(obj , grp=False):
    obj_mat = obj.matrix.copy()
    return obj_mat

def get_instances(obj_parent, scene, prefix = None, level = 0):
    crn_scn = scene.corona
    dupli_list = []
    if use_ob_mb( obj_parent, scene):
        dupli_list = sample_mblur( obj_parent, scene, dupli = True)
    else:
        obj_parent.dupli_list_create( scene)
        grouped_by_name = {}
        dupliobj = None
        for dupli in obj_parent.dupli_list:
            debug('| ' * level + "Dupli Object", dupli.object, dupli.object.type, dupli.object.dupli_type, dupli.object.is_duplicator)
            if dupli.object.is_duplicator and dupli.object.dupli_type in {'VERTS', 'FACES', 'GROUP'}:
                nested_duplis = get_instances(dupli.object, scene, None, level + 1)
            #     nested_name = prefix + '\\' + dupli.object.name if prefix else dupli.object.name
            #     debug('| ' * level + "Nested Duplicator " + nested_name)
            #     nested_duplis = get_instances(dupli.object, scene, nested_name, level + 1)
                debug('| ' * level, "Nested Duplicator ", dupli.object.name, str(nested_duplis))
                dupli_list.extend(nested_duplis)
            else:
                name = dupli.object.data.name
                if name not in grouped_by_name:
                    dupliobj = dupli.object
                    matrices = []
                    duplis = (dupliobj, matrices, obj_parent)
                    grouped_by_name[name] = duplis
                else:
                    (dupliobj, matrices, obj_parent) = grouped_by_name[name]
                debug('| ' * level + "Mesh " + name)
                dupli_matrix = get_matrix( dupli, grp=(obj_parent.dupli_type == 'GROUP'))
                matrices.append(dupli_matrix)

        for (dupliobj, matrices, obj_parent) in grouped_by_name.values():
            dupli_list.append( [ dupliobj, matrices, obj_parent])
        obj_parent.dupli_list_clear()
    return dupli_list

def is_psys_emitter( ob):
    emitter = False
    if hasattr( ob, "modifiers"):
        for mod in ob.modifiers:
            if mod.type == 'PARTICLE_SYSTEM' and mod.show_render:
                psys = mod.particle_system
                if psys.settings.render_type == 'OBJECT' and psys.settings.dupli_object is not None:
                    emitter = True
                    break
                if psys.settings.render_type == 'GROUP' and psys.settings.dupli_group is not None:
                    emitter = True
                    break
    return emitter

def render_emitter(ob):
    render = False
    for psys in ob.particle_systems:
        if psys.settings.use_render_emitter:
            render = True
            break
    return render

def is_obj_visible(scene, obj, is_dupli=False):
    ov = False
    for lv in [ol and sl and rl for ol,sl,rl in zip(obj.layers, scene.layers, scene.render.layers.active.layers)]:
        ov |= lv
    return (ov or is_dupli) and not obj.hide_render

def get_instance_materials(ob, crn_scn):
    obmats = []
    default_mat = None
    if crn_scn.material_override and not ob.corona.override_exclude:
        if crn_scn.clay_render:
            default_mat = 'Default_corona_blender'
        else:
            default_mat = bpy.data.materials[crn_scn.override_material].name
    # Grab materials attached to object instances ...
    if hasattr(ob, 'material_slots'):
        for ms in ob.material_slots:
            if ms.material:
                obmats.append(default_mat if default_mat else ms.material.name)
    # ... and to the object's mesh data
    if len(obmats) == 0 and hasattr(ob, 'data') and hasattr(ob.data, 'materials'):
        for m in ob.data.materials:
            if m:
                obmats.append(default_mat if default_mat else m.name)
    return obmats

def has_hairsys( ob):
    has_hair = False
    for mod in ob.modifiers:
        if mod.type == 'PARTICLE_SYSTEM' and mod.show_render:
            psys = mod.particle_system
            if psys.settings.type == 'HAIR' and psys.settings.render_type == 'PATH':
                has_hair = True
                break
    return has_hair

def use_def_mb( ob, scene):
    return scene.corona.use_def_mblur and ob.corona.use_mblur and ob.corona.mblur_type == 'deform'

def use_ob_mb( ob, scene):
    return scene.corona.use_ob_mblur and ob.corona.use_mblur and ob.corona.mblur_type == 'object'


def sample_mblur( ob, scene, dupli = False):
    matrices = []
    frame_orig = scene.frame_current
    frame_set = scene.frame_set

    if ob.type == 'CAMERA':
        num_segs = scene.corona.cam_mblur_segments
    else:
        num_segs = scene.corona.ob_mblur_segments
    offset = 0.5 + ( scene.corona.frame_offset / 2)
    interval = scene.render.fps *  ( 1 / scene.corona.shutter_speed)
    start = frame_orig - ( interval * ( 1 - offset))
    end = frame_orig + ( interval * offset)
    frac = interval / (num_segs + 1)
    if not dupli:
        # Set frames and collect matrices.
        # Have to set subframe separately.
        frame_set( int(start), subframe = ( start % 1))
        if ob.type == 'CAMERA':
            matrices.append( calc_cam_shift( ob, scene))
        else:
            matrices.append( ob.matrix_world.copy())
        frame = start
        for i in range( 0, (num_segs + 1)):
            frame += frac
            frame_set( int( frame), subframe = ( frame % 1))
            if ob.type == 'CAMERA':
                matrices.append( calc_cam_shift( ob, scene))
            else:
                matrices.append( ob.matrix_world.copy())
    elif dupli:
        frame_set( int(start), subframe = ( start % 1))
        ob.dupli_list_create( scene)
        for dupli in ob.dupli_list:
            matrices.append( [dupli.object, [dupli.matrix.copy(),]])
        ob.dupli_list_clear()
        frame = start
        for i in range( 0, (num_segs + 1)):
            frame += frac
            frame_set( int( frame), subframe = ( frame % 1))
            ob.dupli_list_create( scene)
            for m in range( 0, len(ob.dupli_list)):
                matrices[m][1].append( ob.dupli_list[m].matrix.copy())
            ob.dupli_list_clear()
    # Reset timeline and return.
    frame_set( frame_orig)
    return matrices

def get_psys_instances(ob, scene):
    '''
    Return a dictionary of
    particle: [dupli.object, [matrices]]
    pairs. This function assumes particle systems and
    face / verts duplication aren't being used on the same object.
    '''




    all_dupli_obs = {}
    current_total = 0
    index = 0
    use_mblur = use_ob_mb( ob, scene)
    if not hasattr(ob, 'modifiers'):
        return {}

    if not use_mblur:
        # If no motion blur
        ob.dupli_list_create(scene, settings = 'RENDER')
        ob_mat_list = [(dob.object, [dob.matrix.copy()]) for dob in ob.dupli_list]
        ob.dupli_list_clear()
        return ob_mat_list

    for modifier in ob.modifiers:
        if modifier.type == 'PARTICLE_SYSTEM' and modifier.show_render:
            psys = modifier.particle_system
            if not psys.settings.render_type in {'OBJECT', 'GROUP'}:
                continue

            # steps = psys.settings.draw_step
            # steps = 2**steps + 1
            # num_parents = len(psys.particles)
            # num_children = len(psys.child_particles)

            # print("num parents: " + str(num_parents))
            # print("num children: " + str(num_children))
            # debug(psys.point_cache)

            # for pindex in range(0, num_parents + num_children):
            #     print("particle " + str(pindex))

            #     particle = None
            #     if pindex >= num_parents:
            #        particle = psys.particles[(pindex - num_parents) % num_parents]
            #     else:
            #        particle = psys.particles[pindex]
            #     # debug(psys.child_particles[pindex].location)
            #     for step in range(0, steps):
            #         co = psys.co_hair(ob, pindex, step)
            #         print("co: " + str(step) + ": " + str(co) + " " + str(particle.location))


            psys.set_resolution( scene, ob, 'RENDER')
            if psys.settings.type == 'EMITTER':
                particles = [p for p in psys.particles if
                (psys.settings.show_unborn and p.alive_state == 'UNBORN') or
                (psys.settings.use_dead and (p.alive_state == 'DEAD' or p.alive_state == 'DYING')) or
                p.alive_state == 'ALIVE']
            else:
                particles = [p for p in psys.particles]


            start = current_total
            current_total += len( particles)

            debug( "Start: %d, Current total: %d" % ( start, current_total))
            debug( "Particles: %d, Duplis: %d" % (len(particles), len(ob.dupli_list)))
            # if not use_mblur:
            #     # No motion blur.

            #     dupli_obs = []
            #     # end_dupli = min(current_total, len(ob.dupli_list))
            #     for dupli_index in range( start, current_total ):
            #         # Store the dupli.objects for the current particle system only
            #         # ob.dupli_list is created in descending order of particle systems
            #         # So it should be reliable to match them this way.
            #         dupli_obs.append( ob.dupli_list[dupli_index].object)
            #     if psys.settings.type == 'EMITTER':
            #         p_dupli_obs_pairs = list( zip( particles, dupli_obs))     # Match the particles to the dupli_obs
            #         p_dupli_obs_dict = {p[0]:[ p[1], []] for p in p_dupli_obs_pairs}  # Create particle:[dupli.object, [matrix]] pairs
            #         for particle, (dupli_obj, tmats) in p_dupli_obs_dict.items():
            #             size = particle.size
            #             loc = particle.location
            #             rotmat = particle.rotation.to_matrix().to_4x4()
            #             locmat = mathutils.Matrix.Translation(loc)
            #             scalemat = mathutils.Matrix.Scale(size, 4, (1,0,0))
            #             scalemat *= mathutils.Matrix.Scale(size, 4, (0,1,0))
            #             scalemat *= mathutils.Matrix.Scale(size, 4, (0,0,1))
            #             mat = (locmat * rotmat * scalemat) * dupli_obj.matrix_world
            #             tmats.append(mat)
            #     else:
            #         dupli_mat_list = [ dupli.matrix.copy() for dupli in ob.dupli_list[ start:current_total]]
            #         p_dupli_obs_pairs = list( zip( particles, dupli_obs, dupli_mat_list))
            #         p_dupli_obs_dict = { p[0]:[ p[1], [p[2]]] for p in p_dupli_obs_pairs}
            # else:
                # Using motion blur
            p_dupli_obs_dict, index = sample_psys_mblur( ob, scene, psys, index, start, current_total)

            # Add current particle system to the collection.
            all_dupli_obs.update( p_dupli_obs_dict)
            psys.set_resolution( scene, ob, 'PREVIEW')

    if not use_mblur:
        # If no motion blur
        ob.dupli_list_clear()
    return all_dupli_obs


def sample_psys_mblur( ob, scene, psys, index, start, current_total):
    '''
    Return a dictionary of
    particle: [dupli.object, [matrices]]
    pairs
    '''
    p_dupli_obs_dict = {}
    frame_orig = scene.frame_current
    frame_set = scene.frame_set

    num_segs = scene.corona.ob_mblur_segments
    offset = 0.5 + ( scene.corona.frame_offset / 2)
    interval = scene.render.fps *  ( 1 / scene.corona.shutter_speed)
    frame_start = frame_orig - ( interval * ( 1 - offset))
    #frame_end = frame_orig + ( interval * offset)
    frac = interval / ( num_segs + 1)

    frame_set( int( frame_start), subframe = ( frame_start % 1))
    if psys.settings.type == 'EMITTER':
        particles = [p for p in psys.particles if p.alive_state == 'ALIVE']
    else:
        particles = [p for p in psys.particles]
    if len(particles) == 0:
        return False

    # Create dupli list.
    ob.dupli_list_create( scene, settings = 'RENDER')

    frame = frame_start

    if psys.settings.type == 'EMITTER':
        # Emitter particle system.
        #ob.dupli_list_create( scene, 'RENDER')
        duplis = [dupli.object for dupli in ob.dupli_list]
        p_duplis_pairs = list( zip( particles, duplis))
        p_dupli_obs_dict = {p[0]:[ p[1], []] for p in p_duplis_pairs}
        new_index = index
        for f in range( 0, ( num_segs + 1)):
            new_index = index
            frame += frac
            frame_set( int( frame), subframe = ( frame % 1))
            for particle in p_dupli_obs_dict.keys():
                size = particle.size
                transl = particle.location
                scale = p_dupli_obs_dict[particle][0].scale * size
                transl = mathutils.Matrix.Translation(( transl))
                scale = mathutils.Matrix.Scale( scale.x, 4, (1,0,0)) * mathutils.Matrix.Scale( scale.y, 4, (0,1,0)) * mathutils.Matrix.Scale( scale.z, 4, (0,0,1))
                mat = transl * scale
                p_dupli_obs_dict[particle][1].append( mat)
                new_index += 1
        index += new_index
        del scale, transl, mat
        ob.dupli_list_clear()

    else:
        # Hair particle system.
        duplis = []
        #ob.dupli_list_create( scene, 'RENDER')
        for dupli_index in range( start, current_total):
            duplis.append( ob.dupli_list[dupli_index].object)
        p_duplis_pairs = list( zip( particles, duplis))
        p_dupli_obs_dict = {p[0]:[ p[1], []] for p in p_duplis_pairs}
        ob.dupli_list_clear()
        new_index = index
        for f in range( 0, (num_segs + 1)):
            new_index = index
            frame += frac
            # Move to next frame, create dupli_list.
            frame_set( int( frame), subframe = ( frame % 1))
            ob.dupli_list_create( scene, settings = 'RENDER')
            for particle in p_dupli_obs_dict.keys():
                mat = ob.dupli_list[new_index].matrix.copy()
                p_dupli_obs_dict[particle][1].append(mat)
                new_index += 1
            ob.dupli_list_clear()
        index += new_index

    frame_set( frame_orig)
    return p_dupli_obs_dict, index


def write_transform( file, scene, obj):
    '''
    Write transforms for objects to .scn file.
    '''
    scnw = file.write
    crn_scn = scene.corona
    if obj.type == 'CAMERA':
        # Camera transformations.
        if crn_scn.use_cam_mblur:
            # Camera motion blur.
            mat_list = sample_mblur( obj, scene)
            for ob_mat in mat_list:
                for i in range( 0, 3):
                    scnw( '{0} {1} {2} {3} '.format( ob_mat[i][0],
                                               ob_mat[i][1],
                                               ob_mat[i][2],
                                               ob_mat[i][3]))
        else:
            # No camera motion blur.
            ob_mat = calc_cam_shift( obj, scene)
            for i in range( 0, 3):
                scnw( '{0} {1} {2} {3} '.format(ob_mat[i][0],
                                               ob_mat[i][1],
                                               ob_mat[i][2],
                                               ob_mat[i][3]))
    else:
        # Object transformations.
        if use_ob_mb( obj, scene):
            # Transformation motion blur.
            mat_list = sample_mblur( obj, scene)
            scnw( 'transformA %d ' % crn_scn.ob_mblur_segments)
            for ob_mat in mat_list:
                for i in range( 0, 3):
                    scnw( '{0} {1} {2} {3} '.format( ob_mat[i][0],
                                                   ob_mat[i][1],
                                                   ob_mat[i][2],
                                                   ob_mat[i][3]))
        else:
            # No transformation motion blur.
            ob_mat = obj.matrix_world.copy()
            scnw( 'transform ')
            for i in range( 0, 3):
                scnw( '{0} {1} {2} {3} '.format(ob_mat[i][0],
                                               ob_mat[i][1],
                                               ob_mat[i][2],
                                               ob_mat[i][3]))
        scnw( '\n')

def plugin_path():
    return os.path.dirname(realpath(__file__))

def resolution(scene):
    xr = scene.render.resolution_x * scene.render.resolution_percentage / 100.0
    yr = scene.render.resolution_y * scene.render.resolution_percentage / 100.0
    return xr, yr

def calc_fov(camera_ob, width, height):
    '''
    Calculate horizontal FOV if rendered height is greater than rendered with
    Thanks to NOX exporter developers for this and the next solution
    '''
    camera_angle = degrees(camera_ob.data.angle)
    if width < height:
        length = 18.0/tan(camera_ob.data.angle/2)
        camera_angle = 2*atan(18.0*width/height/length)
        camera_angle = degrees(camera_angle)
    return camera_angle

def calc_cam_shift(camera_ob, scene):
    return camera_ob.matrix_world.copy()


def colorTemperature2rgb(kelvin):
    return temp_data.temp2rgb(kelvin, True)

#find group node based on slot id - doesn't work properly if group node is nested
def find_group_node(nodes, input_identifier):
    try:
        current_nodes = nodes
        protection = 0
        identifier = input_identifier
        while True and protection < 10:
            protection += 1
            group_nodes = []
            for i in range(len(current_nodes)):
                if current_nodes[i].type == 'GROUP':
                    group_nodes.append(current_nodes[i])
                    for j in range(len(current_nodes[i].inputs)):
                        if current_nodes[i].inputs[j].identifier == identifier:
                            return [current_nodes[i], current_nodes[i].inputs[j]]



            current_nodes = group_nodes
            if len(group_nodes) == 0:
                return [None, None]


    except Exception as e:
        print(e)
