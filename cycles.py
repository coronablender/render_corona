import bpy
from .util         import debug
from .properties   import nodes
import math

def migrate_cycles_nodes():
    addon = bpy.context.user_preferences.addons.get(__package__, None)
    if addon and not addon.preferences.convert_cycles:
        return
    materials = bpy.data.materials
    debug("Migrating cycles nodes", len(materials))
    for material in materials:
        migrate_cycles_material(material)

def migrate_cycles_material(material):
        if not material.use_nodes or not material.corona.use_nodes:
            return
        node_tree = material.node_tree
        nodes = material.node_tree.nodes
        cycles_output = None
        corona_output = None
        ymin = 100000
        ymax = -100000
        to_convert = []
        for node in nodes.values():
            if node.bl_idname.startswith("Corona"):
                corona_output = node
                break
            if node.name == "CoronaUVMappingPreviewNode":
                continue

            ymin = min(node.location[1] - node.dimensions[1], ymin)
            ymax = max(node.location[1], ymax)
            if node.bl_idname == "ShaderNodeOutputMaterial":
                cycles_output = node
                continue

            if node.bl_idname in node_map.keys():
                to_convert.append(node)

        # This node tree has been migrated already
        if corona_output:
            return

        # We need a cycles output to copy
        if not cycles_output or not cycles_output.inputs[0].is_linked:
            return

        # Some padding
        ymin -= 100

        corona_output = nodes.new("CoronaOutputNode")
        corona_output.select = False
        corona_output.location = [cycles_output.location[0], ymin - (ymax - cycles_output.location[1])]

        corona_material = nodes.new("CoronaMtlNode")
        corona_material.select = False
        corona_material.location = [cycles_output.location[0] - 200, ymin - (ymax - cycles_output.location[1])]

        node_tree.links.new(corona_material.outputs[0], corona_output.inputs[0])

        node_replacements = {}

        for node in to_convert:
            node_name, convert_func = node_map[node.bl_idname]
            replacement_node = nodes.new(node_name)
            replacement_node.select = False
            replacement_node.location = [node.location[0], ymin - (ymax - node.location[1])]
            node_replacements[node.name] = replacement_node.name
            convert_func(node, replacement_node)

        convert_corona_input(node_replacements, node_tree, cycles_output, corona_material, cycles_output.inputs[0])


def convert_corona_input(node_replacements, node_tree, cycles_node, corona_material, input, active_input = None):
    # Should only be one because this is an input
    if not input.is_linked:
        return

    link = input.links[0]
    node = link.from_node
    if active_input and node.name in node_replacements.keys():
        node_tree.links.new(node_tree.nodes[node_replacements[node.name]].outputs[0], active_input)
    if node.bl_idname == "ShaderNodeBsdfDiffuse":
        color = node.inputs["Color"]
        corona_material.inputs["Diffuse Color"].default_value = color.default_value[0:3]
        convert_corona_input(node_replacements, node_tree, node, corona_material, color, active_input = corona_material.inputs["Diffuse Color"])
        corona_material.inputs["Diffuse Level"].default_value = 1
    if node.bl_idname == "ShaderNodeBsdfGlossy":
        color = node.inputs["Color"]
        roughness = node.inputs["Roughness"]
        corona_material.inputs["Reflect Color"].default_value = color.default_value[0:3]
        convert_corona_input(node_replacements, node_tree, node, corona_material, color, active_input = corona_material.inputs["Reflect Color"])
        corona_material.inputs["Reflect Level"].default_value = 1
        corona_material.inputs["Fresnel"].default_value = 5
        corona_material.inputs["Reflect Gloss"].default_value = 1 - roughness.default_value
    if node.bl_idname == "ShaderNodeBsdfAnisotropic":
        color = node.inputs["Color"]
        roughness = node.inputs["Roughness"]
        anisotropy = node.inputs["Anisotropy"]
        rotation = node.inputs["Rotation"]
        corona_material.inputs["Reflect Color"].default_value = color.default_value[0:3]
        convert_corona_input(node_replacements, node_tree, node, corona_material, color, active_input = corona_material.inputs["Reflect Color"])
        corona_material.inputs["Reflect Level"].default_value = 1
        corona_material.inputs["Fresnel"].default_value = 5
        corona_material.inputs["Reflect Gloss"].default_value = 1 - roughness.default_value
        corona_material.inputs["Anisotropy"].default_value = (anisotropy.default_value / 2) + 0.5
        corona_material.inputs["Anisotropy Rotation"].default_value = 0.5 - rotation.default_value
    if node.bl_idname == "ShaderNodeMixShader":
        convert_corona_input(node_replacements, node_tree, node, corona_material, node.inputs[1])
        convert_corona_input(node_replacements, node_tree, node, corona_material, node.inputs[2])
        convert_corona_input(node_replacements, node_tree, node, corona_material, node.inputs[0])
    if node.bl_idname == "ShaderNodeFresnel":
        ior = node.inputs["IOR"].default_value
        corona_material.inputs["Fresnel"].default_value = ior
    if node.bl_idname == "ShaderNodeEmission":
        color = node.inputs["Color"]
        strength = node.inputs["Strength"]
        corona_material.inputs["Emission Color"].default_value = color.default_value[0:3]
        convert_corona_input(node_replacements, node_tree, node, corona_material, color, active_input = corona_material.inputs["Emission Color"])
        corona_material.inputs["Multiplier"].default_value = strength.default_value
    if node.bl_idname == "ShaderNodeBsdfGlass":
        color = node.inputs["Color"]
        roughness = node.inputs["Roughness"]
        ior = node.inputs["IOR"]
        corona_material.inputs["Refract Level"].default_value = 1
        corona_material.inputs["Refract Color"].default_value = color.default_value[0:3]
        convert_corona_input(node_replacements, node_tree, node, corona_material, color, active_input = corona_material.inputs["Refract Color"])
        corona_material.inputs["Refract Gloss"].default_value = 1 - roughness.default_value
        convert_corona_input(node_replacements, node_tree, node, corona_material, roughness, active_input = corona_material.inputs["Refract Gloss"])
        corona_material.inputs["Refract IOR"].default_value = ior.default_value
        convert_corona_input(node_replacements, node_tree, node, corona_material, ior, active_input = corona_material.inputs["Refract IOR"])
        corona_material.inputs["Reflect Level"].default_value = 1
        corona_material.inputs["Fresnel"].default_value = ior.default_value
        corona_material.inputs["Reflect Gloss"].default_value = 1 - roughness.default_value
    if node.bl_idname == "ShaderNodeBsdfRefraction":
        color = node.inputs["Color"]
        roughness = node.inputs["Roughness"]
        ior = node.inputs["IOR"]
        corona_material.inputs["Refract Level"].default_value = 1
        corona_material.inputs["Refract Color"].default_value = color.default_value[0:3]
        convert_corona_input(node_replacements, node_tree, node, corona_material, color, active_input = corona_material.inputs["Refract Color"])
        corona_material.inputs["Refract Gloss"].default_value = 1 - roughness.default_value
        convert_corona_input(node_replacements, node_tree, node, corona_material, roughness, active_input = corona_material.inputs["Refract Gloss"])
        corona_material.inputs["Refract IOR"].default_value = ior.default_value
        convert_corona_input(node_replacements, node_tree, node, corona_material, ior, active_input = corona_material.inputs["Refract IOR"])


def convert_tex_node(cycles_node, node):
    if cycles_node.image:
        if cycles_node.image.packed_file:
            cycles_node.image.unpack()
        node.set_image(cycles_node.image)
    if 'NONE' == cycles_node.color_space:
        node.use_gamma = True
        node.gamma = 1

def convert_empty(cycles_node, node):
    pass

node_map = {
    "ShaderNodeTexImage":("CoronaTexNode", convert_tex_node),
    "ShaderNodeAmbientOcclusion":("CoronaAONode", convert_empty)
}
