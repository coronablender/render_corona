import bpy
from . import material, object, particle, passes, render, world, camera, lamp
from ..util import CrnUpdate

def CrnAddPanel( element):
    print( "Adding UI Panel:", element)

# Use some of the existing Blender UI elements.

import bl_ui.properties_scene as properties_scene
properties_scene.SCENE_PT_scene.COMPAT_ENGINES.add( 'CORONA')
properties_scene.SCENE_PT_audio.COMPAT_ENGINES.add( 'CORONA')
properties_scene.SCENE_PT_physics.COMPAT_ENGINES.add( 'CORONA')
properties_scene.SCENE_PT_keying_sets.COMPAT_ENGINES.add( 'CORONA')
properties_scene.SCENE_PT_keying_set_paths.COMPAT_ENGINES.add( 'CORONA')
properties_scene.SCENE_PT_unit.COMPAT_ENGINES.add( 'CORONA')
properties_scene.SCENE_PT_custom_props.COMPAT_ENGINES.add( 'CORONA')
del properties_scene

import bl_ui.properties_world as properties_world
properties_world.WORLD_PT_context_world.COMPAT_ENGINES.add( 'CORONA')
del properties_world

import bl_ui.properties_material as properties_material
properties_material.MATERIAL_PT_context_material.COMPAT_ENGINES.add( 'CORONA')
properties_material.MATERIAL_PT_custom_props.COMPAT_ENGINES.add( 'CORONA')
del properties_material

# import bl_ui.properties_texture as properties_texture
# INCLUDE_TEXTURE = [ 'TEXTURE_MT_specials', 'TEXTURE_PT_context_texture', 'TEXTURE_PT_image', 'TEXTURE_UL_texslots', 'Panel', 'Object', 'Material', 'Texture', 'TextureSlotPanel', 'TextureButtonsPanel', 'UIList', 'id_tex_datablock', 'context_tex_datablock']
# for member in dir(properties_texture):
#     if member in INCLUDE_TEXTURE:
#         subclass = getattr(properties_texture, member)
#         try:
#             subclass.COMPAT_ENGINES.add('CORONA')
#         except:
#             pass
# del properties_texture

import bl_ui.properties_texture as properties_texture
for member in dir( properties_texture):
    subclass = getattr( properties_texture, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except: pass
del properties_texture

import bl_ui.properties_data_lamp as properties_data_lamp
properties_data_lamp.DATA_PT_context_lamp.COMPAT_ENGINES.add( 'CORONA')
# properties_data_lamp.DATA_PT_lamp.COMPAT_ENGINES.add( 'CORONA')
del properties_data_lamp


import bl_ui.properties_data_camera as properties_data_camera
properties_data_camera.DATA_PT_context_camera.COMPAT_ENGINES.add('CORONA')
properties_data_camera.CAMERA_MT_presets.COMPAT_ENGINES.add('CORONA')

properties_data_camera.DATA_PT_camera_display.COMPAT_ENGINES.add('CORONA')

del properties_data_camera

# Enable all existing panels for these contexts
import bl_ui.properties_data_mesh as properties_data_mesh
for member in dir( properties_data_mesh):
    subclass = getattr( properties_data_mesh, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except: pass
del properties_data_mesh

import bl_ui.properties_object as properties_object
for member in dir( properties_object):
    subclass = getattr( properties_object, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except: pass
del properties_object

import bl_ui.properties_data_mesh as properties_data_mesh
for member in dir( properties_data_mesh):
    subclass = getattr( properties_data_mesh, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except: pass
del properties_data_mesh

import bl_ui.properties_particle as properties_particle
for member in dir( properties_particle):
    subclass = getattr( properties_particle, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_particle

import bl_ui.properties_physics_cloth as properties_physics_cloth
for member in dir( properties_physics_cloth):
    subclass = getattr( properties_physics_cloth, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_cloth

import bl_ui.properties_physics_common as properties_physics_common
for member in dir( properties_physics_common):
    subclass = getattr( properties_physics_common, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_common

import bl_ui.properties_physics_dynamicpaint as properties_physics_dynamicpaint
for member in dir( properties_physics_dynamicpaint):
    subclass = getattr( properties_physics_dynamicpaint, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_dynamicpaint

import bl_ui.properties_physics_field as properties_physics_field
for member in dir( properties_physics_field):
    subclass = getattr( properties_physics_field, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_field

import bl_ui.properties_physics_fluid as properties_physics_fluid
for member in dir( properties_physics_fluid):
    subclass = getattr( properties_physics_fluid, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_fluid

import bl_ui.properties_physics_rigidbody as properties_physics_rigidbody
for member in dir( properties_physics_rigidbody):
    subclass = getattr( properties_physics_rigidbody, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_rigidbody

import bl_ui.properties_physics_rigidbody_constraint as properties_physics_rigidbody_constraint
for member in dir( properties_physics_rigidbody_constraint):
    subclass = getattr( properties_physics_rigidbody_constraint, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_rigidbody_constraint

import bl_ui.properties_physics_smoke as properties_physics_smoke
for member in dir( properties_physics_smoke):
    subclass = getattr( properties_physics_smoke, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_smoke

import bl_ui.properties_physics_softbody as properties_physics_softbody
for member in dir( properties_physics_softbody):
    subclass = getattr( properties_physics_softbody, member)
    try: subclass.COMPAT_ENGINES.add( 'CORONA')
    except:  pass
del properties_physics_softbody

def register():
    CrnAddPanel( "Corona render settings")
    render.register()
    CrnAddPanel( "camera")
    camera.register()
    CrnAddPanel( "material")
    material.register()
    CrnAddPanel( "environment")
    world.register()
    CrnAddPanel( "particle systems")
    particle.register()
    CrnAddPanel( "Corona render passes")
    passes.register()
    CrnAddPanel( "Corona object properties")
    object.register()
    CrnAddPanel( "lamp")
    lamp.register()

def unregister():
    material.unregister()
    particle.unregister()
    passes.unregister()
    object.unregister()
    render.unregister()
    camera.unregister()
    lamp.unregister()
    world.unregister()

