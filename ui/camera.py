import bpy

#---------------------------------------
# Camera UI
#---------------------------------------
class CoronaCameraPanel( bpy.types.Panel):
    bl_label = "Corona Camera"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    COMPAT_ENGINES = {'CORONA'}
    bl_context = "data"

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA' and context.active_object.type == 'CAMERA'

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        crn_scene_props = scene.corona
        cam_data = context.object.data
        crn_cam_props = context.active_object.data.corona
        cam_props = context.object.data

        layout.prop(crn_cam_props, "camera_type", text = 'Camera')
        if crn_cam_props.camera_type == 'perspective':
            # Generic camera settings
            row = layout.row()
            row.prop(cam_props, "lens_unit")
            if cam_props.lens_unit == 'FOV':
                row.prop(cam_props, "angle")
            else:
                row.prop(cam_props, "lens")

            layout.prop(crn_cam_props, "use_dof")

            box = layout.box()

            row = box.row()
            row.prop(context.active_object.data, "dof_distance", text = "Focal distance")
            row.active = context.active_object.data.dof_object is None

            row = box.row()
            row.prop(context.active_object.data, "dof_object", text = 'Focal object')

            box.separator()

            # DoF / Exposure settings
            box.label( "Basic Settings:")
            row = box.row()
            row.prop(crn_cam_props, "camera_dof")
            row.prop( crn_scene_props, "colmap_iso")

            row = box.row()
            row.prop( crn_scene_props, "shutter_speed")
            row.prop(context.object.data, "sensor_width")

            box.separator()

            # Aperture shape
            box.label( "Aperture Shape (bokeh):")
            row = box.row()
            row.prop( crn_cam_props, "bokeh_blades")
            row.prop( crn_cam_props, "bokeh_rotation")

        elif crn_cam_props.camera_type == 'ortho':
            layout.prop(crn_cam_props, "ortho_width")

        elif crn_cam_props.camera_type == 'cylindrical':
            layout.prop(crn_cam_props, "cylindrical_height")

        elif crn_cam_props.camera_type == 'spherical' or crn_cam_props.camera_type == 'cubemap':
            layout.prop( crn_cam_props, "use_vr")
            if crn_cam_props.use_vr:
                layout.prop(crn_cam_props, "vr_eyeSeparation")
                layout.prop(crn_cam_props, "vr_frontOffset")
                layout.prop(crn_cam_props, "vr_convergenceDistance")

        layout.separator()

        # Render region settings
        layout.prop( crn_cam_props, "render_clipping")

        row = layout.row( align=True)
        row.active = crn_cam_props.render_clipping
        row.prop( cam_props, "clip_start")
        row.prop( cam_props, "clip_end")

        row = layout.row( align = True)
        row.prop( cam_props, "shift_x")
        row.prop( cam_props, "shift_y")

        layout.separator()
        layout.prop( crn_cam_props, "use_region")

#---------------------------------------
# Motion Blur UI
#---------------------------------------
class CoronaMBPanel( bpy.types.Panel):
    bl_label = "Corona Motion Blur"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    COMPAT_ENGINES = {'CORONA'}
    bl_context = "data"

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine in CoronaMBPanel.COMPAT_ENGINES and context.active_object.type == 'CAMERA'

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        row = layout.row()
        row.prop( crn_props, "use_cam_mblur", text = "Enable Camera")
        if crn_props.use_cam_mblur:
            row.prop( crn_props, "cam_mblur_segments")

        row = layout.row()
        row.prop( crn_props, "use_ob_mblur", text = "Enable Transform")
        if crn_props.use_ob_mblur:
            row.prop( crn_props, "ob_mblur_segments", text = "Segments")

        # layout.prop( crn_props, "use_def_mblur")
        layout.prop( crn_props, "frame_offset")


def register():
    bpy.utils.register_class( CoronaCameraPanel)

def unregister():
    bpy.utils.unregister_class( CoronaCameraPanel)
