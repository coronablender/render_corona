#!/bin/bash

(

oldversion=`cat version.txt`

if [[ $# -eq 0 ]] ; then
    echo Pass in next version e.g. setversion.sh 5.4.3
    echo Current Version: ${oldversion}
    exit 0
fi

newversion=$1
input=`echo $1 | tr '.' ' '`
set -- $input
bmajor=$1
bminor=$2
bbuild=$3

tokens=`cat version.txt | tr '.' ' '`
set -- $tokens
amajor=$1
aminor=$2
abuild=$3

echo Old Version: $amajor.$aminor.$abuild
echo New Version: $bmajor.$bminor.$bbuild

sed -i '' -e s/$oldversion/$newversion/g version.txt

sed -i '' -e s/\($amajor,\ $aminor,\ $abuild\)/\($bmajor,\ $bminor,\ $bbuild\)/g __init__.py

sed -i '' -e s/updater\.fake_install\ \=\ True/updater\.fake_install\ \=\ False/g addon_updater_ops.py


)

