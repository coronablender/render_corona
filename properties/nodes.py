import bpy, math
from bpy.types          import NodeTree, Node
from bpy.app.handlers   import persistent
from nodeitems_utils    import NodeCategory, NodeItem
from ..util             import addon_dir, strip_spaces, realpath, join_names_underscore, print_exception
from ..util             import filter_flatten_params, debug, CrnInfo, view3D_mat_update, getTransform
from .material          import CoronaMatProps, CoronaTexProps
from .object            import CoronaObjects
import nodeitems_utils
import os
import mathutils
import time
import traceback
import hashlib

import xml.etree.cElementTree as ElementTree
from xml.etree.cElementTree import Element, Comment, SubElement

def current_milli_time():
    return int(time.time() * 1000)

def getNodePtr(node, postfix = ''):
    name_pointer = join_names_underscore( node.name, str(node.as_pointer()))
    if len(node.label) != 0:
        name_pointer = join_names_underscore( node.label, name_pointer)
    if len(postfix) != 0:
        name_pointer = join_names_underscore( name_pointer, postfix)
    return "%s_%d" % (name_pointer, time.time())

def getMat(root, name, type = 'Native'):
    results = root.findall(".//*[@name='%s']" % name)
    # debug("Results", results)
    if len(results) == 0:
        xml_mat_def = Element('materialDefinition', name=name)
        xml_mat = SubElement(xml_mat_def, 'material')
        xml_mat.set('class', type)
        return xml_mat, xml_mat_def
    else:
        return None, None

def getMap(root, name, clazz):
    results = root.findall(".//*[@name='%s']" % name)
    if len(results) == 0:
        xml_map_def = Element('mapDefinition', name=name)

        if clazz != None:
            xml_map = SubElement(xml_map_def, 'map')
            xml_map.set('class', clazz)
        else:
            xml_map = None

        return xml_map, xml_map_def
    else:
        return None, None

def addSocket(root, parent, node, name, xmlname, inline = False):
    try:
        socket = node.inputs[name]
        params = None
        if socket.is_linked and len(socket.links) > 0:
            params = socket.get_socket_params(node, root, inline)
        if params is not None:
            xml = SubElement(parent, xmlname)
            xml.append(params)
            # val = socket.get_socket_value(node, root)
            # if val != None:
            #     xml.text = str(val)
            return xml
        else:
            val = socket.get_socket_value(node, root)
            if val != None:
                xml = SubElement(parent, xmlname)
                xml.text = str(val)

                return xml
    except:
        traceback.print_exc()
        pass

def addChild(root, parent, node, name, inline = False):
    socket = node.inputs[name]
    if socket.is_linked and len(socket.links) != 0:
        childValue = socket.get_socket_params(node, root, inline)
        if childValue is not None:
            parent.append(childValue)
    else:
        val = socket.get_socket_value(node, root)
        if val != None:
            parent.text = str(val)

def getReference(name):
    el = Element('material')
    el.text = name
    el.set('class', 'Reference')
    return el

def getMapReference(name):
    el = Element('map')
    el.text = name
    el.set('class', 'Reference')
    return el

#--------------------------------
# Base class for Corona nodes.
#--------------------------------

class CoronaNode(bpy.types.Node):
    bl_label = 'Output'

    def copy( self, node):
        pass

    def update( self):
        view3D_mat_update(self, bpy.context)

    def draw_label( self):
        return self.bl_label

    @classmethod
    def poll(cls, ntree):
        if hasattr(ntree, 'bl_idname'):
            return ntree.bl_idname == 'ShaderNodeTree'
        else:
            return True

    def some_linked( self, *args):
        return any([self.inputs[name].is_linked or len(self.inputs[name].links) > 0 for name in args])

#--------------------------------
# Image texture shader node.
#--------------------------------
class CoronaTexNode(CoronaNode):
    '''Corona Image Texture Node'''
    bl_idname = "CoronaTexNode"
    bl_label = "Image Texture"
    bl_icon = 'TEXTURE'

    tex_path = bpy.props.StringProperty( name = "Texture",
                                         description = "Path to the image texture",
                                         default = '',
                                         subtype = 'FILE_PATH')
    use_gamma = bpy.props.BoolProperty( name = "Use Gamma",
                                        description = "When disabled will automatically use 2.2 for LDR and 1.0 for HDR",
                                        default = False)

    gamma = bpy.props.FloatProperty( name = "Gamma",
                                     default = 2.2)

    init_texture_name = bpy.props.StringProperty(   name = "init_texture_name",
                                                    description = "Name of the texture slot",
                                                    default = '')

    #node start
    def init( self, context):
        #depreciated, use normal map node instead
        self.inputs.new( 'CoronaFloatUnits', 'Bump Strength')
        self.inputs['Bump Strength'].hide = True
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketColor', "Color")
        self.outputs.new( 'CoronaFloat', "Alpha")

        self.__create_texture_slot(self.name)


    def draw_buttons( self, context, layout):
        if bpy.data.textures.get(self.init_texture_name):
            layout.template_ID( bpy.data.textures[self.init_texture_name], 'image', open='image.open')
        else:
            layout.label("Image:")
            layout.prop( self, "tex_path", text = "")

        op = None
        row = layout.row()
        if bpy.data.textures.get(self.init_texture_name):
            for node in context.object.active_material.node_tree.nodes:
                if node.name.startswith("CoronaUVMappingPreviewNode"):
                    if bpy.data.textures[self.init_texture_name].image == node.image:
                        op = row.operator( "corona.refresh_both", icon = "RESTRICT_VIEW_OFF")
                        op.node = self.name
                        break

        if not op:
            op = row.operator( "corona.refresh_both", icon = "RESTRICT_VIEW_ON")
            op.node = self.name

        layout.prop( self, "use_gamma")
        if self.use_gamma:
            layout.prop( self, "gamma")


    #node duplicated
    def copy(self, node):
        if self.name != node.name:
            prev_name = self.init_texture_name
            self.__create_texture_slot(node.name)

            newTex = bpy.data.textures.get(self.init_texture_name, None)
            if newTex:
                oldTex = bpy.data.textures.get(prev_name, None)
                newTex.image = oldTex.image


    #node removed
    def free(self):
        if bpy.data.textures.get(self.init_texture_name):
            bpy.data.textures[self.init_texture_name].image = None
            slots = bpy.context.active_object.active_material.texture_slots
            for i in range(len(slots)):
                if slots[i].name == self.init_texture_name:
                    slots.clear(i)
                    break

    #---------------------------------------------------------------------------
    def __create_texture_slot(self, node_name):
        sha = hashlib.sha1()
        sha.update( (str(time.time()) + node_name).encode('utf8'))
        self.init_texture_name = ".Corona@" + sha.hexdigest()[0:10]
        tex = bpy.data.textures.new(self.init_texture_name, 'IMAGE')

        try:
            slot = bpy.context.active_object.active_material.texture_slots.add()
            slot.texture = tex
        except:
            pass

    #used by material converter
    def set_image(self, image):
        tex = bpy.data.textures.get(self.init_texture_name)
        if tex:
            bpy.data.textures[self.init_texture_name].image = image

        if image != None:
            self.tex_path = image.filepath
        else:
            self.tex_path = ''

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        try:
            if bpy.data.textures.get(self.init_texture_name):
                if bpy.data.textures[self.init_texture_name].image != None:
                    self.tex_path = bpy.data.textures[self.init_texture_name].image.filepath


            if self.tex_path != "":

                name_pointer = getNodePtr(self)
                xml_map, xml_map_def = getMap(root, name_pointer, 'Texture')
                if xml_map != None:
                    SubElement(xml_map, 'image').text = realpath(self.tex_path)
                    if self.use_gamma:
                        SubElement(xml_map, 'gamma').text = '%.3f' % (self.gamma)

                    #depreciated, use normal map node instead
                    if "Bump Strength" in self.inputs:
                        addSocket(root, xml_map, self, "Bump Strength", "bumpStrength", inline)

                    addChild(root, xml_map, self, "UV Map", True)

                    if not inline and xml_map_def: root.append(xml_map_def)

                if source == 'Alpha':
                    name_pointer_alpha = getNodePtr(self, source)
                    xml_map_alpha, xml_map_def_alpha = getMap(root, name_pointer_alpha, 'Channel')
                    if xml_map_alpha != None:
                        child = SubElement(xml_map_alpha, 'child')
                        SubElement(xml_map_alpha, 'rSource').text = 'A'
                        SubElement(xml_map_alpha, 'gSource').text = 'A'
                        SubElement(xml_map_alpha, 'bSource').text = 'A'
                        SubElement(xml_map_alpha, 'alphaSource').text = 'A'
                        SubElement(xml_map_alpha, 'monoSource').text = 'A'
                        if inline:
                            child.append(xml_map)
                            xml_map = xml_map_alpha
                        else:
                            # if xml_map:
                            #     root.append(xml_map)
                            child.append(getMapReference(name_pointer))
                        xml_map = xml_map_alpha

                        if not inline and xml_map_def_alpha: root.append(xml_map_def_alpha)

                if inline:
                    return xml_map
                else:
                    if source != "Color":
                        name_pointer = getNodePtr(self, source)
                    return getMapReference(name_pointer)

            return None
        except Exception as e:
            print_exception(e)

    def get_texture_path(self):
        try:
            if bpy.data.textures.get(self.init_texture_name):
                if bpy.data.textures[self.init_texture_name].image != None:
                    self.tex_path = bpy.data.textures[self.init_texture_name].image.filepath
            return self.tex_path
        except Exception:
            return False

class CoronaNormalMapNode(CoronaNode):
    bl_idname = "CoronaNormalMapNode"
    bl_label = "Normal Map"

    #node start
    def init( self, context):
        self.inputs.new( 'CoronaStrength', 'Strength')
        self.inputs.new( 'CoronaLinkedMap', 'Color')
        self.outputs.new( 'CoronaNormalMap', "Normal OpenGL")
        self.outputs.new( 'CoronaNormalMap', "Normal DirectX")
        self.outputs.new( 'CoronaNormalMap', "Bump")

    def get_node_params( self, root, inline = False, source = None):
        try:
            name_pointer = getNodePtr(self)
            if len(self.inputs['Color'].links) > 0:
                if source == "Normal OpenGL" or source == "Normal DirectX":
                    xml_map, xml_map_def = getMap(root, name_pointer, 'Normal')

                    child = SubElement(xml_map, 'child')
                    child_map = SubElement(child, 'map')
                    child_map.set('class', 'Mix')

                    #mix -> strength below 1
                    child_map_operation = SubElement(child_map, 'operation')
                    child_map_operation.text = 'mix'
                    child_map_a = SubElement(child_map, 'a')
                    child_map_a.text = "0.5 0.5 1.0 1.0"

                    #mul -> strength above 1 -> doesn't give expected results though
                    child_map_b = SubElement(child_map, 'b')
                    child_map_b_map = SubElement(child_map_b, 'map')
                    child_map_b_map.set('class', 'Mix')

                    child_map_b_map_operation = SubElement(child_map_b_map, 'operation')
                    child_map_b_map_operation.text = 'mul'

                    #invert (x * (-1) + 1) green channel if directX
                    child_map_b_map_a = SubElement(child_map_b_map, 'a')
                    child_map_b_map_a_map = SubElement(child_map_b_map_a, 'map')
                    child_map_b_map_a_map.set('class', 'Mix')

                    child_map_b_map_a_map_operation = SubElement(child_map_b_map_a_map, 'operation')
                    child_map_b_map_a_map_operation.text = 'add'

                    child_map_b_map_a_map_a = addSocket(root, child_map_b_map_a_map, self, "Color", "a", inline = True)
                    child_map_b_map_a_map_b = SubElement(child_map_b_map_a_map, 'b')
                    if source == "Normal DirectX":
                        child_map_b_map_a_map_b.text = "0.0 -1.0 0.0 0.0"
                    else:
                        child_map_b_map_a_map_b.text = "0.0 0.0 0.0 0.0"

                    child_map_b_map_b = SubElement(child_map_b_map, 'b')
                    strength = self.inputs['Strength'].default_value
                    if source == "Normal DirectX":
                        color_b = [strength, strength * (-1), 1.0, 1.0]
                    else:
                        color_b = [strength, strength, 1.0, 1.0]
                    child_map_b_map_b.text = str(color_b[0]) + ' ' + str(color_b[1]) + ' ' + str(color_b[2]) + ' ' + str(color_b[3])

                    child_map_amountB = SubElement(child_map, 'amountB')
                    child_map_amountB.text = str(self.inputs['Strength'].get_socket_real_value())

                    #linear workflow for normal maps if gamma not set
                    for elem in xml_map_def.iterfind(".//*[@class='%s']" % "Texture"):
                        if len(elem.findall(".//gamma")) == 0:
                            gamma = SubElement(elem, 'gamma')
                            gamma.text = str(1.0000)


                if source == "Bump":
                    xml_map, xml_map_def = getMap(root, name_pointer, None)
                    prev_xml_map = self.inputs['Color'].get_socket_params(self, root, inline = True)
                    xml_map_def.append(prev_xml_map)

                    for elem in xml_map_def.iterfind(".//*[@class='%s']" % "Texture"):
                        if len(elem.findall(".//bumpStrength")) == 0:
                            bump_strength = SubElement(elem, 'bumpStrength')
                        else:
                            bump_strength = elem.find(".//bumpStrength")

                        bump_strength.text = str(self.inputs['Strength'].get_socket_real_value() / 100)

                # if child_map_b_map_a_map_a:
                if not inline: root.append(xml_map_def)

                if inline:
                    return xml_map
                else:
                    return getMapReference(name_pointer)
        except Exception as e:
            print_exception(e)

class CoronaFalloffNode(CoronaNode):
    '''Corona Falloff Node'''
    bl_idname = "CoronaFalloffNode"
    bl_label = "Falloff"
    bl_icon = 'SMOOTH'

    mode = bpy.props.EnumProperty( name = "Falloff Mode",
                                        description = "Which geometry frame is the Axis defined in",
                                        default = 'view',
                                        items = [
                                            ('view', 'View', 'Axis is relative to camera using this the axis is usually 0 0 1'),
                                            ('local', 'Local', 'Axis is local relative to object'),
                                            ('world', 'World', 'Axis is world relative')])

    axis = bpy.props.FloatVectorProperty( name = "Axis",
                                description = "",
                                default = (0.0, 0.0, 1.0),
                                subtype = "TRANSLATION")

    abs_value = bpy.props.BoolProperty( name = "Absolute Value",
                                    description = "",
                                    default = False)

    def init( self, context):
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "mode", text = "Mode")
        layout.prop( self, "axis", text = "Axis")
        layout.prop( self, "abs_value", text = "Absolute Value")

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Falloff')
        if xml_mat != None:
            SubElement(xml_mat, 'mode').text = self.mode
            SubElement(xml_mat, 'axis').text = "%.3f %.3f %.3f" % self.axis[:]
            SubElement(xml_mat, 'absValue').text = "True" if self.abs_value else "False"
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaDataNode(CoronaNode):
    '''Corona Data Node'''
    bl_idname = "CoronaDataNode"
    bl_label = "Data"
    bl_icon = 'SMOOTH'

    mode = bpy.props.EnumProperty( name = "Data Mode",
                                        description = "Geometry data recast as a color",
                                        default = 'UVW',
                                        items = [
                                            ('UVW', 'UVW', ''),
                                            ('shadingNormal', 'Shading Normal', ''),
                                            ('geometryNormal', 'Geometry Normal', ''),
                                            ('dotProduct', 'Dot Product', ''),
                                            ('zDepth', 'Z Depth', ''),
                                            ('dUvw', 'D UVW', '')])

    def init( self, context):
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "mode", text = "Mode")

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Data')
        if xml_mat != None:
            SubElement(xml_mat, 'mode').text = self.mode
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaGradientNode(CoronaNode):
    '''Corona Gradient Node'''
    bl_idname = "CoronaGradientNode"
    bl_label = "Gradient"
    bl_icon = 'SMOOTH'

    mode = bpy.props.EnumProperty( name = "Gradient Mode",
                                        description = "",
                                        default = 'radial',
                                        items = [
                                            ('u', 'U', 'Creates a gradient in the u axis by directly returning the u value as intensity'),
                                            ('v', 'V', 'Creates a gradient in the v axis by directly returning the v value as intensity'),
                                            ('radial', 'Radial', 'Creates a radial gradient in the 0-1 UVW square with center in 0.5, 0.5')])

    def init( self, context):
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "mode", text = "")

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Gradient')
        if xml_mat != None:
            SubElement(xml_mat, 'mode').text = self.mode
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaRoundEdgesNode(CoronaNode):
    '''Corona RoundEdges Node'''
    bl_idname = "CoronaRoundEdgesNode"
    bl_label = "Round Edges"
    bl_icon = 'SMOOTH'

    max_radius = bpy.props.FloatProperty( name = "Max radius",
                                         description = "Radius of the rounded corners effect",
                                         min = 0,
                                         default = 0)
    samples = bpy.props.IntProperty( name = "Samples",
                                     description = "Number of samples to take. It influences the render noise and speed.",
                                     default = 0,
                                     min = 0)

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMap', 'Mapped Max Radius')
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "max_radius", text = "Max Radius")
        layout.prop( self, "samples", text = "Samples")

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'RoundEdges')
        if xml_mat != None:
            SubElement(xml_mat, 'maxRadius').text = "%.3f" % self.max_radius
            if self.samples != 0:
                SubElement(xml_mat, 'samples').text = "%d" % self.samples
            if self.inputs["Mapped Max Radius"].is_linked == True:
                addChild(root, xml_mat, self, "Mapped Max Radius", True)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# AO shader node.
#--------------------------------
class CoronaAONode(CoronaNode):
    '''Corona AO Shader Node'''
    bl_idname = "CoronaAONode"
    bl_label = "AO Shader"
    bl_icon = 'SMOOTH'

    distance = bpy.props.FloatProperty( name = "Distance",
                                        description = "Maximum distance rays travel before being considered not occluded",
                                        default = 10,
                                        min = 0,
                                        soft_max = 100)

    # invert = bpy.props.BoolProperty( name = "Invert",
    #                                     description = "Causes the rays to be shot below normal, not above, resulting in computing the occlusion below normal",
    #                                     default = False)

    samples = bpy.props.IntProperty( name = "Samples",
                                        description = "Quality of ambient occlusion calculation",
                                        default = 50,
                                        min = 1,
                                        max = 2048)

    phong_exponent = bpy.props.FloatProperty( name = "Phong Exponent",
                                        description = "How concentrated should be the rays around normal",
                                        default = 1)

    mix_exponent = bpy.props.FloatProperty( name = "Mix Exponent",
                                        description = "Sets balance between occluded and unoccluded color (works similarly to gamma correction)",
                                        default = 1)

    normal_mode = bpy.props.EnumProperty( name = "Normal Mode",
                                        description = "Where to compute the effect - on the outside/inside of the object",
                                        default = 'both',
                                        items = [
                                            ('both', 'Both', 'Both sides used'),
                                            ('outside', 'Outside', 'Outside is used'),
                                            ('inside', 'Inside', 'Inside is used')])

    exclude_mode = bpy.props.EnumProperty( name = "Occlude Mode",
                                        description = "Self occlusion, other occlusion, include/exclude or all",
                                        default = 'all',
                                        items = [
                                            ('all', 'All', 'All objects contribute to occlusion results'),
                                            ('same', 'Same', 'Only self-occlusion'),
                                            ('other', 'Other', 'No self-occlusion'),
                                            ('list', 'List', 'Include/Exclude used for occlusion.')])

    offset = bpy.props.FloatVectorProperty( name = "Offset",
                                description = "Constant offset in world space added to all generated rays. This can be used to shift the distribution of rays in one constant direction (typically up or down to create for example water weathering)",
                                default = (0.0, 0.0, 0.0),
                                subtype = "TRANSLATION")

    def init( self, context):
        self.inputs.new( 'CoronaAoDistMap', "Distance Map")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.inputs.new( 'CoronaObjectList', "Include")
        self.inputs.new( 'CoronaObjectList', "Exclude")
        self.outputs.new( 'NodeSocketFloat', "Occluded")

    def draw_buttons( self, context, layout):
        # layout.prop( self, "invert")
        layout.prop( self, "samples")
        layout.prop( self, "distance")
        layout.prop( self, "phong_exponent")
        layout.prop( self, "mix_exponent")
        layout.prop( self, "normal_mode")
        layout.prop( self, "exclude_mode")
        layout.prop( self, "offset")

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Ao')
        if xml_mat != None:
            SubElement(xml_mat, 'maxDistance').text = '%.3f' % self.distance
            # SubElement(xml_mat, 'invert').text = 'true' if self.invert else 'false'
            SubElement(xml_mat, 'samples').text = '%d' % self.samples
            SubElement(xml_mat, 'phongExponent').text = '%.3f' % self.phong_exponent
            SubElement(xml_mat, 'mixExponent').text = '%.3f' % self.mix_exponent
            if self.exclude_mode == 'same':
                SubElement(xml_mat, 'excludeMode').text = 'other'
            elif self.exclude_mode == 'other':
                SubElement(xml_mat, 'excludeMode').text = 'same'
            elif self.exclude_mode != 'all':
                SubElement(xml_mat, 'excludeMode').text = self.exclude_mode
            SubElement(xml_mat, 'normalMode').text = self.normal_mode
            SubElement(xml_mat, 'offset').text = '%.3f %.3f %.3f' % self.offset[0:3]
            addSocket(root, xml_mat, self, "Distance Map", "mappedDistance", inline)
            addChild(root, xml_mat, self, "UV Map", True)

            if self.exclude_mode == 'list' and ("Include" in self.inputs and self.inputs["Include"].is_linked) or ("Exclude in inputs" and self.inputs["Exclude"].is_linked):
                xml_inex = SubElement(xml_mat, 'includeExclude')
                if self.inputs["Include"].is_linked:
                    includes = self.inputs["Include"].get_socket_params(self, root, inline)
                    for inc in includes:
                        SubElement(xml_inex, 'included').text = inc.name

                if self.inputs["Exclude"].is_linked:
                    excludes = self.inputs["Exclude"].get_socket_params(self, root, inline)
                    for excl in excludes:
                        SubElement(xml_inex, 'excluded').text = excl.name

            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

def updateUVIndex(self, context):
    if self.map_channel_name:
        self.map_channel = context.object.data.uv_textures.find(self.map_channel_name)
    else:
        self.map_channel = -1

class CoronaUVWNode(CoronaNode):
    '''Corona UV Shader Node'''
    bl_idname = "CoronaUVWNode"
    bl_label = "UV Map"
    bl_icon = 'SMOOTH'

    mode = CoronaTexProps.uv_mode
    map_channel = CoronaTexProps.uv_map_channel
    map_channel_name = bpy.props.StringProperty(name="UV Map", update=updateUVIndex)
    scale = CoronaTexProps.uv_scale
    offset = CoronaTexProps.uv_offset
    rotate_z = CoronaTexProps.uv_rotate_z
    enviro_rotate = CoronaTexProps.uv_enviro_rotate
    enviro_mode = CoronaTexProps.uv_enviro_mode
    wrap_mode_u = CoronaTexProps.uv_wrap_mode_u
    wrap_mode_v = CoronaTexProps.uv_wrap_mode_v
    blur = CoronaTexProps.uv_blur
    use_real_scale = CoronaTexProps.uv_use_real_scale

    def init( self, context):
        self.outputs.new( 'CoronaUVW', "UV Map")

    def draw_buttons( self, context, layout):
        layout.prop( self, "mode")
        layout.label('Must use binary object output', icon='INFO')
        # layout.prop( self, "map_channel")
        layout.prop_search( self, "map_channel_name", context.object.data, "uv_textures")
        if self.map_channel != -1 and not self.map_channel_name:
            layout.label("Old Map Index: %d" % self.map_channel, icon='ERROR')
        layout.prop( self, "scale")
        layout.prop( self, "offset")
        layout.prop( self, "rotate_z")
        layout.prop( self, "enviro_rotate")
        layout.prop( self, "enviro_mode")
        layout.prop( self, "wrap_mode_u")
        layout.prop( self, "wrap_mode_v")
        layout.prop( self, "blur")
        layout.prop( self, "use_real_scale")

    def socket_value_update( self, context):

        debug("Current map channel", self.map_channel_name)
        if not self.map_channel_name and self.map_channel != -1:
            debug("Setting map channel name", self.map_channel)
            self.map_channel_name = context.object.data.uv_textures[self.map_channel]
            debug("Found map channel", context.object.data.uv_textures[self.map_channel])

    def get_node_params( self, root, inline = False, source = None):

        # name_pointer = getNodePtr(self)
        # xml_mat, xml_mat_def = getMap(root, name_pointer, 'Mix')
        xml_uv = Element('uvMap')

        SubElement(xml_uv, 'mode').text = self.mode
        if self.map_channel != -1:
            SubElement(xml_uv, 'mapChannel').text = '%d' % self.map_channel
        SubElement(xml_uv, 'scale').text = '%.3f %.3f %.3f' % self.scale[0:3]
        SubElement(xml_uv, 'offset').text = '%.3f %.3f %.3f' % self.offset[0:3]
        SubElement(xml_uv, 'rotateZ').text = '%.3f' % (self.rotate_z * 180 / math.pi)
        SubElement(xml_uv, 'enviroRotate').text = '%.3f' % (self.enviro_rotate * 180 / math.pi + 180)
        SubElement(xml_uv, 'enviroMode').text = self.enviro_mode
        SubElement(xml_uv, 'wrapModeU').text = self.wrap_mode_u
        SubElement(xml_uv, 'wrapModeV').text = self.wrap_mode_v
        if self.blur != 0:
            SubElement(xml_uv, 'blur').text = '%.3f' % self.blur
        SubElement(xml_uv, 'useRealWorldScale').text = 'True' if self.use_real_scale else 'False'

        return xml_uv

def updateNumPoints(self, context):
    old_num_items = len(self.inputs) - 2
    if old_num_items < self.num_items:
        for x in range(old_num_items, self.num_items):
            self.inputs.new( 'CoronaPoint', "Point %d" % (x+1))
        old_num_items = self.num_items
    if old_num_items > self.num_items:
        for x in range(old_num_items, self.num_items, -1):
            self.inputs.remove( self.inputs["Point %d" % x])
        old_num_items = self.num_items
    return None
#--------------------------------
# Curve shader node.
#--------------------------------
class CoronaCurveNode(CoronaNode):
    '''Corona Curve Shader Node'''
    bl_idname = "CoronaCurveNode"
    bl_label = "Curve Shader"
    bl_icon = 'SMOOTH'

    num_items = bpy.props.IntProperty(name = 'Number of points', default=2, min=2, max=10, update=updateNumPoints)

    # curve_data = bpy.props.StringProperty( name = 'Curve data')

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Input")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.inputs.new( 'CoronaPoint', "Point 1")
        self.inputs.new( 'CoronaPoint', "Point 2")
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons(self, context, layout):
        layout.prop(self, 'num_items', 'Points')

        # layout.template_curve_mapping( self, 'curve_data', type='VECTOR', levels=False, brush=False, use_negative_slope=False )

    def get_node_params( self, root, inline = False, source = None):

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Curve')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Input", "child", inline)
            for x in range(1, self.num_items+1):
                xml_mat.append(self.inputs["Point %d" % x].get_socket_value(self, root))
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

def updateNumPointColors(self, context):
    old_num_items = len(self.inputs) - 2
    if old_num_items < self.num_items:
        for x in range(old_num_items, self.num_items):
            self.inputs.new( 'CoronaPointColor', "Point %d" % (x+1))
        old_num_items = self.num_items
    if old_num_items > self.num_items:
        for x in range(old_num_items, self.num_items, -1):
            self.inputs.remove( self.inputs["Point %d" % x])
        old_num_items = self.num_items
    return None
#--------------------------------
# Interpolation shader node.
#--------------------------------
class CoronaInterpolationNode(CoronaNode):
    '''Corona Interpolation Node'''
    bl_idname = "CoronaInterpolationNode"
    bl_label = "Interpolation Shader"
    bl_icon = 'SMOOTH'

    num_items = bpy.props.IntProperty(name = 'Number of points', default=2, min=2, max=10, update=updateNumPointColors)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Input")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.inputs.new( 'CoronaPointColor', "Point 1")
        self.inputs.new( 'CoronaPointColor', "Point 2")
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons(self, context, layout):
        layout.prop(self, 'num_items', 'Points')

    def get_node_params( self, root, inline = False, source = None):

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Interpolation')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Input", "child", inline)
            for x in range(1, self.num_items+1):
                xml_mat.append(self.inputs["Point %d" % x].get_socket_value(self, root))
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Normal shader node.
#--------------------------------
class CoronaNormalNode(CoronaNode):
    '''Corona Normal Shader Node'''
    bl_idname = "CoronaNormalNode"
    bl_label = "Normal Shader"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Child")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketShader', "Color")

    def get_node_params( self, root, inline = False, source = None):

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Normal')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Child", "child", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# FrontBack shader node.
#--------------------------------
class CoronaFrontBackNode(CoronaNode):
    '''Corona FrontBack Shader Node'''
    bl_idname = "CoronaFrontBackNode"
    bl_label = "FrontBack Shader"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Front")
        self.inputs.new( 'CoronaColor', "Back")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'CoronaColor', "Color")

    def get_node_params( self, root, inline = False, source = None):

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'FrontBack')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Front", "front", inline)
            addSocket(root, xml_mat, self, "Back", "back", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

def getChannelEnum(default, name):
    return bpy.props.EnumProperty( name = name,
                                description = "Select source for this channel",
                                default = default,
                                items = [
                                    ('R', 'R', 'Red channel of the input'),
                                    ('G', 'G', 'Green channel of the input'),
                                    ('B', 'B', 'Blue channel of the input'),
                                    ('A', 'A', 'Alpha channel of the input'),
                                    ('RgbIntensity', 'RGB Intensity', 'Average of Red, Green and Blue of the input'),
                                    ('One', 'One', 'Output channel is set to the value of 1.0'),
                                    ('Zero', 'Zero', 'Output channel is set to the value of 0.0')])

#--------------------------------
# Channel shader node.
#--------------------------------
class CoronaChannelNode(CoronaNode):
    '''Corona Channel Shader Node'''
    bl_idname = "CoronaChannelNode"
    bl_label = "Channel Shader"
    bl_icon = 'SMOOTH'

    rsource = getChannelEnum('R', 'Red Source')
    gsource = getChannelEnum('G', 'Green Source')
    bsource = getChannelEnum('B', 'Blue Source')
    asource = getChannelEnum('A', 'Alpha Source')
    monosource = getChannelEnum('RgbIntensity', 'Mono Source')

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Child")
        self.inputs.new( 'CoronaLinked', "UV Map")
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'rsource')
        layout.prop(self, 'gsource')
        layout.prop(self, 'bsource')
        layout.prop(self, 'asource')
        layout.prop(self, 'monosource')

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Channel')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Child", "child", inline)
            if self.rsource != 'Zero':
                SubElement(xml_mat, 'rSource').text = self.rsource
            if self.gsource != 'Zero':
                SubElement(xml_mat, 'gSource').text = self.gsource
            if self.bsource != 'Zero':
                SubElement(xml_mat, 'bSource').text = self.bsource
            if self.asource != 'Zero':
                SubElement(xml_mat, 'alphaSource').text = self.asource
            if self.monosource != 'Zero':
                SubElement(xml_mat, 'monoSource').text = self.monosource
            addSocket(root, xml_mat, self, "UV Map", "uvMap", inline)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Tone Map shader node.
#--------------------------------
class CoronaToneMapNode(CoronaNode):
    '''Corona Normal Shader Node'''
    bl_idname = "CoronaToneMapNode"
    bl_label = "Tone Map Shader"
    bl_icon = 'SMOOTH'

    invert = bpy.props.BoolProperty(name = 'Invert', description = 'output = 1 - output', default = False)
    clamp = bpy.props.BoolProperty(name = 'Clamp', description = 'output = clamp(output, 0, 1)', default = False)
    absolute = bpy.props.BoolProperty(name = 'Absolute', description = 'output = abs(output)', default = False)
    remove = bpy.props.BoolProperty(name = 'Remove Tone Mapping', description = 'output = inverseImageToneMapping(output)', default = False)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Child")
        self.inputs.new( 'CoronaMultiply', "Multiplier")
        self.inputs.new( 'CoronaFloat', "Alpha Multiplier")
        self.inputs.new( 'CoronaOffset', "Offset")
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'invert')
        layout.prop(self, 'clamp')
        layout.prop(self, 'absolute')
        layout.prop(self, 'remove')

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'ToneMap')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Child", "child", inline)
            addSocket(root, xml_mat, self, "Multiplier", "multiplier", inline)
            addSocket(root, xml_mat, self, "Alpha Multiplier", "alphaMultiplier", inline)
            addSocket(root, xml_mat, self, "Offset", "offset", inline)
            SubElement(xml_mat, 'invert').text = 'True' if self.invert else 'False'
            SubElement(xml_mat, 'clamp').text = 'True' if self.clamp else 'False'
            SubElement(xml_mat, 'abs').text = 'True' if self.absolute else 'False'
            SubElement(xml_mat, 'removeToneMapping').text = 'True' if self.remove else 'False'
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

def updateMultiNumItems(self, context):
    old_num_items = int((len(self.inputs) - 6) / 2)
    num_items = self.num_items
    if old_num_items < num_items:
        for x in range(old_num_items, num_items, 1):
            self.inputs.new( 'CoronaColor', "Item %d" % (x+1))
            self.inputs.new( 'CoronaWeight', "Weight %d" % (x+1))
        old_num_items = num_items
    if old_num_items > num_items:
        for x in range(old_num_items, num_items, -1):
            self.inputs.remove( self.inputs["Item %d" % x])
            self.inputs.remove( self.inputs["Weight %d" % x])
        old_num_items = num_items
    return None

#--------------------------------
# Multi shader node.
#--------------------------------
class CoronaMultiNode(CoronaNode):
    '''Corona Multi Shader Node'''
    bl_idname = "CoronaMultiNode"
    bl_label = "Multi Shader"
    bl_icon = 'SMOOTH'

    num_items = bpy.props.IntProperty(name = 'Number of submaps', default=2, min=2, soft_max=10, update=updateMultiNumItems)
    mode = bpy.props.EnumProperty(name = 'Mode', default  = 'Material', items = [
        ('Material', 'Material', ''),
        ('MaterialId', 'Material Id', ''),
        ('Instance', 'Instance', ''),
        ('Primitive', 'Primitive', '')
        ])

    def init( self, context):
        self.inputs.new( 'CoronaSeed', "Seed")
        self.inputs.new( 'CoronaFloat010', "Mix Min")
        self.inputs.new( 'CoronaFloat011', "Mix Max")
        self.inputs.new( 'CoronaFloat010', "Hue Randomization")
        self.inputs.new( 'CoronaFloat010', "Gamma Randomization")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.inputs.new( 'CoronaColor', "Item 1")
        self.inputs.new( 'CoronaWeight', "Weight 1")
        self.inputs.new( 'CoronaColor', "Item 2")
        self.inputs.new( 'CoronaWeight', "Weight 2")
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'mode')
        layout.prop(self, 'num_items')

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Multi')

        if xml_mat != None:
            SubElement(xml_mat, 'mode').text = self.mode
            addSocket(root, xml_mat, self, "Seed", "seed", inline)
            addSocket(root, xml_mat, self, "Mix Min", "mixMin", inline)
            addSocket(root, xml_mat, self, "Mix Max", "mixMax", inline)
            for x in range(1, self.num_items+1):
                slot = SubElement(xml_mat, 'slot')
                addSocket(root, slot, self, "Item %d" % x, "item", inline)
                addSocket(root, slot, self, "Weight %d" % x, "frequency", inline)
            addSocket(root, xml_mat, self, "Hue Randomization", "hueRandomization", inline)
            addSocket(root, xml_mat, self, "Gamma Randomization", "gammaRandomization", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Mix shader node.
#--------------------------------
def updateMixType(self, context):
    self.inputs["Amount"].hide = self.mix_type != 'mix'
    return None

class CoronaMixNode(CoronaNode):
    '''Corona Mix Shader Node'''
    bl_idname = "CoronaMixNode"
    bl_label = "Mix RGB"
    bl_icon = 'SMOOTH'

    mix_type = bpy.props.EnumProperty( name = "Operation",
                                        description = "Operation to perform on the two color inputs",
                                        default = 'mix',
                                        items = [
                                            ('mix', 'Mix', 'PBR Mix the two nodes = (a * (1 - amount)) + (b * amount)'),
                                            ('add', 'Add', 'Add the two nodes (possibly not PBR) = a + b'),
                                            ('mul', 'Multiply', 'Multiply the two nodes (possibly not PBR) = a * b'),
                                            ('sub', 'Subtract', 'Subtract = a - b')],
                                        update=updateMixType)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Color A")
        self.inputs.new( 'CoronaColor', "Color B")
        self.inputs.new( 'CoronaMixAmount', "Amount")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'mix_type')

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Mix')

        if xml_mat != None:
            SubElement(xml_mat, 'operation').text = self.mix_type

            addSocket(root, xml_mat, self, "Color A", "a", inline)
            addSocket(root, xml_mat, self, "Color B", "b", inline)
            addSocket(root, xml_mat, self, "Amount", "amountB", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Math shader node.
#--------------------------------
def updateMathType(self, context):
    self.inputs["Amount"].hide = self.math_type != 'mix'
    return None

class CoronaMathNode(CoronaNode):
    '''Corona Math Shader Node'''
    bl_idname = "CoronaMathNode"
    bl_label = "Math Shader"
    bl_icon = 'SMOOTH'

    math_type = bpy.props.EnumProperty( name = "Operation",
                                        description = "Operation to perform on the two color inputs",
                                        default = 'mix',
                                        items = [
                                            ('mix', 'Mix', 'PBR Mix the two nodes = (a * (1 - amount)) + (b * amount)'),
                                            ('add', 'Add', 'Add the two nodes (possibly not PBR) = a + b'),
                                            ('mul', 'Multiply', 'Multiply the two nodes (possibly not PBR) = a * b'),
                                            ('sub', 'Subtract', 'Subtract = a - b')],
                                        update=updateMathType)

    def init( self, context):
        self.inputs.new( 'CoronaFloat', "Value A")
        self.inputs.new( 'CoronaFloat', "Value B")
        self.inputs.new( 'CoronaFloat', "Amount")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'CoronaFloat', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'math_type')

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Mix')

        if xml_mat != None:
            SubElement(xml_mat, 'operation').text = self.math_type

            addSocket(root, xml_mat, self, "Value A", "a", inline)
            addSocket(root, xml_mat, self, "Value B", "b", inline)
            addSocket(root, xml_mat, self, "Amount", "amountB", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Fresnel shader node.
#--------------------------------
class CoronaFresnelNode(CoronaNode):
    '''Corona Fresnel Shader Node'''
    bl_idname = "CoronaFresnelNode"
    bl_label = "Fresnel Shader"
    bl_icon = 'SMOOTH'

    ior = bpy.props.FloatProperty( name = "IOR",
                                    description = "Applies a fresnel curve on mono input provided by a child node (higher IOR = more prominent child color)",
                                    default = 1.333,
                                    min = 1,
                                    max = 10)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Child")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "ior")

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        debug("Outputting fresnel", name_pointer)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Fresnel')

        if xml_mat != None:
            SubElement(xml_mat, 'ior').text = '%.4f' % self.ior
            addSocket(root, xml_mat, self, "Child", "child", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaWireframeNode(CoronaNode):
    '''Corona Wireframe Shader Node'''
    bl_idname = "CoronaWireframeNode"
    bl_label = "Wireframe Shader"
    bl_icon = 'SMOOTH'

    edge_width = bpy.props.FloatProperty( name = "Edge Width",
                                    description = "",
                                    default = 1,
                                    min = 0,
                                    soft_max = 10,
                                    unit = 'LENGTH')

    vertex_width = bpy.props.FloatProperty( name = "Vertex Width",
                                    description = "",
                                    default = 1,
                                    min = 0,
                                    soft_max = 10,
                                    unit = 'LENGTH')

    world_space = bpy.props.BoolProperty( name = "World Space",
                                    description = "If true, then edgeWidth and VertexWidth are in world units. If false, they are in screen units (projected pixels)",
                                    default = False)

    all_edges = bpy.props.BoolProperty( name = "All Edges",
                                    description = "If false then only the base mesh edges are visualized. Base mesh edges are those the user see in Blender. All edges created by Corona (for example in displacement) remain hidden",
                                    default = False)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Base")
        self.inputs.new( 'CoronaColor', "Edge")
        self.inputs.new( 'CoronaColor', "Vertex")
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "edge_width")
        layout.prop( self, "vertex_width")
        layout.prop( self, "world_space")
        layout.prop( self, "all_edges")

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Wire')

        if xml_mat != None:
            SubElement(xml_mat, 'edgeWidth').text = '%.4f' % self.edge_width
            SubElement(xml_mat, 'vertexWidth').text = '%.4f' % self.vertex_width
            SubElement(xml_mat, 'worldSpace').text = 'True' if self.world_space else 'False'
            SubElement(xml_mat, 'allEdges').text = 'True' if self.all_edges else 'False'

            addSocket(root, xml_mat, self, "Base", "base", inline)
            addSocket(root, xml_mat, self, "Edge", "edge", inline)
            addSocket(root, xml_mat, self, "Vertex", "vertex", inline)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)


class CoronaCheckerNode(CoronaNode):
    bl_idname = "CoronaCheckerNode"
    bl_label = "Checker Pattern"
    bl_icon = 'SMOOTH'

    size  = bpy.props.FloatProperty( name = "Scale",
                                description = "Scale of pattern",
                                default = 1.0,
                                subtype = "NONE",
                                soft_min = 0.1)

    def init( self, context):
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "size")

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Checker')
        if xml_mat != None:
            SubElement(xml_mat, 'size').text = '%.3f' % self.size
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaNoiseNode(CoronaNode):
    bl_idname = "CoronaNoiseNode"
    bl_label = "Noise Generator"
    bl_icon = 'SMOOTH'

    type = bpy.props.EnumProperty( name = "Type",
                                        description = "Noise Type",
                                        default = 'perlin',
                                        items = [
                                            ('perlin', 'Perlin', ''),
                                            ('cellular', 'Cellular', '')])

    size  = bpy.props.FloatProperty( name = "Scale",
                                description = "Scale of pattern",
                                default = 1.0,
                                subtype = "NONE",
                                soft_min = 0.1)

    levels  = bpy.props.FloatProperty( name = "Levels",
                                description = "",
                                default = 1.0,
                                subtype = "NONE")

    phase  = bpy.props.FloatProperty( name = "Phase",
                                description = "",
                                default = 0.0,
                                subtype = "NONE")

    def init( self, context):
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "type")
        layout.prop( self, "size")
        layout.prop( self, "levels")
        layout.prop( self, "phase")

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Noise')
        if xml_mat != None:
            SubElement(xml_mat, 'type').text = self.type
            SubElement(xml_mat, 'size').text = '%.3f' % self.size
            SubElement(xml_mat, 'levels').text = '%.3f' % self.levels
            SubElement(xml_mat, 'phase').text = '%.3f' % self.phase
            addChild(root, xml_mat, self, 'UV Map', True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Solid colour node.
#--------------------------------
class CoronaSolidNode(CoronaNode):
    '''Corona Fresnel Shader Node'''
    bl_idname = "CoronaSolidNode"
    bl_label = "Solid Color"
    bl_icon = 'SMOOTH'

    color  = bpy.props.FloatVectorProperty( name = "Color",
                                description = "Color optimised so there is no performance penalty for using",
                                default = (1.0, 1.0, 1.0, 1.0),
                                subtype = "COLOR",
                                size = 4,
                                min = 0.0,
                                max = 1.0)

    def init( self, context):
        self.outputs.new( 'CoronaColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "color")

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Solid')
        if xml_mat != None:
            xml_mat.text = '%.3f %.3f %.3f %.3f' % self.color[:]
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)


#--------------------------------
# RGBA color channel node.
#--------------------------------
class CoronaSeparateRGBNode(CoronaNode):
    bl_idname = "CoronaSeparateRGBNode"
    bl_label = "Separate RGBA"

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMap', "Color")
        self.outputs.new( 'CoronaFloat', "R")
        self.outputs.new( 'CoronaFloat', "G")
        self.outputs.new( 'CoronaFloat', "B")
        self.outputs.new( 'CoronaFloat', "A")

    def draw_buttons( self, context, layout):
        pass

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self, source)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Channel')
        if xml_mat != None:
            # SubElement(xml_mat, 'operation').text = 'mul'
            child = SubElement(xml_mat, 'child')
            # channel = SubElement(xml_mat, 'b')
            # if source == 'R':
            #     SubElement(xml_mat, 'b').text = "%.4f %.4f %.4f %.4f" % (1.0, 0, 0, 0)
            # if source == 'G':
            #     SubElement(xml_mat, 'b').text = "%.4f %.4f %.4f %.4f" % (0, 1.0, 0, 0)
            # if source == 'B':
            #     SubElement(xml_mat, 'b').text = "%.4f %.4f %.4f %.4f" % (0, 0, 1.0, 0)
            # if source == 'A':
            #     SubElement(xml_mat, 'b').text = "%.4f %.4f %.4f %.4f" % (0, 0, 0, 1.0)
            SubElement(xml_mat, 'rSource').text = source
            SubElement(xml_mat, 'gSource').text = source
            SubElement(xml_mat, 'bSource').text = source
            SubElement(xml_mat, 'alphaSource').text = source
            SubElement(xml_mat, 'monoSource').text = source
            addChild(root, child, self, "Color", inline)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# RGBA color channel node.
#--------------------------------
class CoronaCombineRGBNode(CoronaNode):
    bl_idname = "CoronaCombineRGBNode"
    bl_label = "Combine RGB"

    def init( self, context):
        self.inputs.new( 'CoronaR0', "R")
        self.inputs.new( 'CoronaG0', "G")
        self.inputs.new( 'CoronaB0', "B")
        # self.inputs.new( 'CoronaA0', "A")
        self.outputs.new( 'CoronaLinkedMap', "Color")

    def draw_buttons( self, context, layout):
        pass

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Mix')
        if xml_mat != None:
            r, g, b = addRGB(xml_mat)
            addChild(root, addMultiply(r, 1.0, 0, 0), self, 'R', inline)
            addChild(root, addMultiply(g, 0, 1.0, 0), self, 'G', inline)
            addChild(root, addMultiply(b, 0, 0, 1.0), self, 'B', inline)
            # addChild(root, addMultiply(a, 0, 0, 0, 1.0), self, 'A', inline)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)
#--------------------------------
# Ray switch shader node.
#--------------------------------
class CoronaRaySwitchNode(CoronaNode):
    bl_idname = "CoronaRaySwitchNode"
    bl_label = "Ray Switch Shader"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaColor', "GI")
        self.inputs.new( 'CoronaLinkedMap', "Reflect")
        self.inputs.new( 'CoronaLinkedMap', "Refract")
        self.inputs.new( 'CoronaLinkedMap', "Direct")
        self.outputs.new( 'CoronaColor', "Color")

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Ray Switch shader node's parameters as a tuple (list, string) '''
        name_pointer = join_names_underscore( self.name, str(self.as_pointer()))

        xml_mat, xml_mat_def = getMap(root, name_pointer, 'RaySwitcher')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "GI", "base", inline)
            addSocket(root, xml_mat, self, "Reflect", "reflect", inline)
            addSocket(root, xml_mat, self, "Refract", "refract", inline)
            addSocket(root, xml_mat, self, "Direct", "direct", inline)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaOutputNode(CoronaNode):
    '''Corona Output Node'''
    bl_idname = "CoronaOutputNode"
    bl_label = "Corona Output"
    bl_icon = 'SMOOTH'

    viewport_color  = CoronaMatProps.viewport_color

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMaterial', "Material")


    def draw_buttons( self, context, layout):
        layout.prop( self, "viewport_color")

    def get_viewport_color(self):
        return self.viewport_color

    def get_node_params( self, root, inline = False, source = None):
        socket = self.inputs['Material']
        if socket.is_linked:
            return socket.get_socket_params(self, root, inline)


class CoronaShadowcatcherOutputNode(CoronaNode):
    '''Corona Shadow Catcher Output Node'''
    bl_idname = "CoronaShadowcatcherOutputNode"
    bl_label = "Corona Shadowcatcher"
    bl_icon = 'SMOOTH'

    mode = bpy.props.EnumProperty( name = "Shading Mode",
                                        description = "A special fake for compositing. It has a limited subset of the native material, plus some extra options that allows using it for compositing rendered imagery on top of live footage.",
                                        default = 'final',
                                        items = [
                                            ('final', 'Final', 'Final means that both the photo background (defined in emission slot) and the CG footage will be present in the resulting render'),
                                            ('composite', 'Composite', 'Composite means that only the CG footage will be present (but the emission slot will still be used to create shadows/alpha for compositing)'),
                                            ('finalNoAlpha', 'Final No Alpha', '')
                                            ])

    projmode = bpy.props.EnumProperty( name = "Projection Mode",
                                        description = "A special fake for compositing. It has a limited subset of the native material, plus some extra options that allows using it for compositing rendered imagery on top of live footage.",
                                        default = 'none',
                                        items = [
                                            ('none', 'None', 'Textures will be used as-is'),
                                            ('backplate', 'Backplate', 'Textures will be reprojected to create contact shadows (to make the CG footage â€œtouchâ€ the background)'),
                                            ('environment', 'Environment', 'Textures will be reprojected to create contact shadows (to make the CG footage â€œtouchâ€ the background)')
                                            ])

    projAllMaps = bpy.props.BoolProperty( name = "Project All Maps",
                                    description = "The projectionMode will be used also for bump/reflections maps (not only emission)",
                                    default = True)

    shadowAmount = bpy.props.FloatProperty( name = "Shadow Amount",
                                         description = "Modifies the amount of shadows in the picture by changing the albedo of the shadow catcher",
                                         min = 0,
                                         default = 0)

    def init( self, context):
        self.inputs.new( 'CoronaColor', 'Emission')
        self.inputs.new( 'CoronaLinked', 'Offscreen Override')
        self.inputs.new( 'CoronaLinked', "Reflect Color")
        self.inputs.new( 'CoronaReflectGloss', "Reflect Gloss")
        self.inputs.new( 'CoronaReflectFresnel', "Reflect Fresnel")
        self.inputs.new( 'CoronaLinked', "Bump")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons(self, context, layout):
        layout.label("To align with world set UVMap X Scale to -1", icon='INFO')
        layout.prop(self, "mode", "Shading Mode")
        layout.prop(self, "projmode", "Projection Mode")
        layout.prop(self, "projAllMaps", "Project All Maps")
        layout.prop(self, "shadowAmount", "Shadow Amount")

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer, 'ShadowCatcher')
        if xml_mat != None:

            SubElement(xml_mat, 'shadingMode').text = self.mode
            SubElement(xml_mat, 'projectionMode').text = self.projmode
            SubElement(xml_mat, 'projectAllMaps').text = 'True' if self.projAllMaps else 'False'
            SubElement(xml_mat, 'shadowAmount').text = '%.3f' % self.shadowAmount

            addSocket(root, xml_mat, self, "Offscreen Override", "offscreenOverride", inline)
            addSocket(root, xml_mat, self, "Emission", "emission", inline)
            addSocket(root, xml_mat, self, "Bump", "bump", inline)
            if self.inputs["Reflect Color"].is_linked:
                xml_reflect = SubElement(xml_mat, "reflect")
                addSocket(root, xml_reflect, self, "Reflect Color", "color", inline)
                addSocket(root, xml_reflect, self, "Reflect Gloss", "glossiness", inline)
                addSocket(root, xml_reflect, self, "Reflect Fresnel", "ior", inline)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)

class CoronaRaySwitchOutputNode(CoronaNode):
    '''Corona RaySwitch Output Node'''
    bl_idname = "CoronaRaySwitchOutputNode"
    bl_label = "Corona RaySwitch Material"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMaterial', "Normal")
        self.inputs.new( 'CoronaLinkedMaterial', "Reflect")
        self.inputs.new( 'CoronaLinkedMaterial', "Refract")
        self.inputs.new( 'CoronaLinkedMaterial', "Direct")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer, 'Rayswitcher')
        if xml_mat != None:
            addSocket(root, xml_mat, self, "Normal", "normal", inline)
            addSocket(root, xml_mat, self, "Reflect", "reflect", inline)
            addSocket(root, xml_mat, self, "Refract", "refract", inline)
            addSocket(root, xml_mat, self, "Direct", "direct", inline)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)

def updateNumLayers(self, context):
    old_num_items = math.floor((len(self.inputs) - 1) / 2)
    if old_num_items < self.num_items:
        for x in range(old_num_items, self.num_items):
            self.inputs.new( 'CoronaLinkedMaterial', "Layer %d" % (x+1))
            self.inputs.new( 'CoronaLayeredWeight', "Weight %d" % (x+1))
        old_num_items = self.num_items
    if old_num_items > self.num_items:
        for x in range(old_num_items, self.num_items, -1):
            self.inputs.remove( self.inputs["Layer %d" % x])
            self.inputs.remove( self.inputs["Weight %d" % x])
        old_num_items = self.num_items
    return None

class CoronaLayeredOutputNode(CoronaNode):
    '''Corona Layered Output Node'''
    bl_idname = "CoronaLayeredOutputNode"
    bl_label = "Corona Layered Material"
    bl_icon = 'SMOOTH'

    num_items = bpy.props.IntProperty(name = 'Number of layers', default=1, min=1, max=10, update=updateNumLayers)

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMaterial', "Base")
        self.inputs.new( 'CoronaLinkedMaterial', "Layer 1")
        self.inputs.new( 'CoronaLayeredWeight', "Weight 1")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'num_items')

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer, 'Layered')
        if xml_mat != None:
            addSocket(root, xml_mat, self, "Base", "base", inline)
            for x in range(1, self.num_items+1, 1):
                xml_layer = addSocket(root, xml_mat, self, "Layer %d" % x, "layer", inline)
                addSocket(root, xml_layer, self, "Weight %d" % x, "weight", inline)

            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)

class RENDER_UL_Object_slots(bpy.types.UIList):
    def draw_item( self, context, layout, data, item, icon, active_data, active_propname, index):

        if 'DEFAULT' in self.layout_type:
            layout.label( text = item.name, translate=False, icon_value=icon)

class CoronaObjectListNode(CoronaNode):
    '''Corona Object List Node'''
    bl_idname = "CoronaObjectListNode"
    bl_label = "Scene Objects"
    bl_icon = 'SMOOTH'

    includes = bpy.props.CollectionProperty(name = "Included Objects", type = CoronaObjects)

    include_index = bpy.props.IntProperty( name = "Include Index",
                                        description = "",
                                        default = 0,
                                        min = 0,
                                        max = 100)

    def init( self, context):
        self.outputs.new("CoronaObjectList", "Output")

    def draw_buttons( self, context, layout):
        row = layout.row()
        row.template_list( "RENDER_UL_Object_slots", "corona_object_list", self, "includes", self, "include_index")

        row = layout.row(align=True)
        op = row.operator( "corona.add_object", icon = "ZOOMIN")
        op.material = self.name
        op = row.operator( "corona.remove_object", icon = "ZOOMOUT")
        op.material = self.name
        op.index = self.include_index
        row = layout.row(align=True)
        op = row.operator( "corona.add_selected", icon = "ZOOM_ALL")
        op.node = self.name

    def get_node_params( self, root, inline = False, source = None):
        return self.includes


def setHue(self, value):
    self['hue'] = value
    updateHSV(self, bpy.context)
def getHue(self):
    if 'hue' not in self:
        self['hue'] = self.hue
    return self['hue']

def setSat(self, value):
    self['sat'] = value
    updateHSV(self, bpy.context)
def getSat(self):
    if 'sat' not in self:
        self['sat'] = self.sat
    return self['sat']

def setVal(self, value):
    self['val'] = value
    updateHSV(self, bpy.context)
def getVal(self):
    if 'val' not in self:
        self['val'] = self.val
    return self['val']

def setLinear(self, value):
    self['linear'] = value
    updateHSV(self, bpy.context)
def getLinear(self):
    if 'linear' not in self:
        self['linear'] = self.linear
    return self['linear']

def huerotatemat(rot):
    RLUM = 0.3086
    GLUM = 0.6094
    BLUM = 0.0820
    rads = (rot - 0.5) * (math.pi * 2)
    xrot = math.asin(-1/math.sqrt(2))
    yrot = math.asin(1/math.sqrt(3))

    mmat = mathutils.Matrix.Identity(4)
    # Rotate gray vector into positive Z
    mmat = mmat * mathutils.Matrix.Rotation(xrot, 4, 'X')
    mmat = mmat * mathutils.Matrix.Rotation(yrot, 4, 'Y')
    # Calculate luminance sheer
    (lx, ly, lz, lv) = mathutils.Vector((RLUM, GLUM, BLUM, 1)) * mmat
    lsx = lx/lz
    lsy = ly/lz
    # Apply the shear to make luminance horizontal
    mmat = mmat * mathutils.Matrix.Shear('XY', 4, (lsx, lsy))
    # Rotate the hue
    mmat = mmat * mathutils.Matrix.Rotation(-rads, 4, 'Z')
    # Unshear the luminance
    mmat = mmat * mathutils.Matrix.Shear('XY', 4, (-lsx, -lsy))
    # Unrotate gray vector
    mmat = mmat * mathutils.Matrix.Rotation(-yrot, 4, 'Y')
    mmat = mmat * mathutils.Matrix.Rotation(-xrot, 4, 'X')
    return mmat

def updateHSV(self, context):
    # degrees = ((self.hue + 0.5) * 360) % 360
    # cosA = math.cos(math.radians(degrees))
    # sinA = math.sin(math.radians(degrees))

    # http://graficaobscura.com/matrix/index.html
    if self.linear:
        # Linear values
        rwgt = 0.3086
        gwgt = 0.6094
        bwgt = 0.082
    else:
        # Gamma 2.2
        rwgt = 0.299
        gwgt = 0.587
        bwgt = 0.114

    huematrix = huerotatemat(self.hue) #[[1,0,0],[0,1,0],[0,0,1]]
    # huematrix = [[1,0,0],[0,1,0],[0,0,1]]
    # huematrix[0][0] = cosA + (1.0 - cosA) / 3.0
    # huematrix[0][1] = 1./3. * (1.0 - cosA) - math.sqrt(1./3.) * sinA
    # huematrix[0][2] = 1./3. * (1.0 - cosA) + math.sqrt(1./3.) * sinA
    # huematrix[1][0] = 1./3. * (1.0 - cosA) + math.sqrt(1./3.) * sinA
    # huematrix[1][1] = cosA + 1./3.*(1.0 - cosA)
    # huematrix[1][2] = 1./3. * (1.0 - cosA) - math.sqrt(1./3.) * sinA
    # huematrix[2][0] = 1./3. * (1.0 - cosA) - math.sqrt(1./3.) * sinA
    # huematrix[2][1] = 1./3. * (1.0 - cosA) + math.sqrt(1./3.) * sinA
    # huematrix[2][2] = cosA + 1./3. * (1.0 - cosA)
    # for y in range(0, 3):
    #     for x in range (0, 3):
    #         matrix[y][x] =   huematrix[y][0] * matrix[0][x] \
    #                             + huematrix[y][1] * matrix[1][x] \
    #                             + huematrix[y][2] * matrix[2][x]



    satmatrix = mathutils.Matrix.Identity(4)
    sat = 1.0 - self.saturation
    sr = sat * rwgt
    sg = sat * gwgt
    sb = sat * bwgt
    satmatrix[0][0] = sr + self.saturation
    satmatrix[1][0] = sr
    satmatrix[2][0] = sr
    satmatrix[0][1] = sg
    satmatrix[1][1] = sg + self.saturation
    satmatrix[2][1] = sg
    satmatrix[0][2] = sb
    satmatrix[1][2] = sb
    satmatrix[2][2] = sb + self.saturation
    # for y in range(0, 4):
    #     for x in range (0, 4):
    #         matrix[y][x] =   satmatrix[y][0] * matrix[0][x] \
    #                             + satmatrix[y][1] * matrix[1][x] \
    #                             + satmatrix[y][2] * matrix[2][x] \
    #                             + satmatrix[y][3] * matrix[3][x]

    valmatrix = mathutils.Matrix.Identity(4)
    valmatrix[0][0] = self.value #[[self.value,0,0,0],[0,self.value,0,0],[0,0,self.value,0],[0,0,0,1]]
    valmatrix[1][1] = self.value
    valmatrix[2][2] = self.value
    valmatrix[3][3] = self.value
    # for y in range(0, 3):
    #     for x in range (0, 3):
    #         matrix[y][x] =   valmatrix[y][0] * matrix[0][x] \
    #                             + valmatrix[y][1] * matrix[1][x] \
    #                             + valmatrix[y][2] * matrix[2][x]
    # for y in range(0, 3):
    #     for x in range (0, 3):
    #         i = y * 3 + x
    #         self.matrix[i] = matrix[y][x]

    result = huematrix * satmatrix * valmatrix
    for y in range(0, 4):
        for x in range(0, 4):
            self.matrix[y * 4 + x] = result[y][x]


def addRGBA(rgbamix):
    SubElement(rgbamix, 'operation').text = 'add'
    gba = SubElement(rgbamix, 'a')
    r = SubElement(rgbamix, 'b')
    gbamix = SubElement(gba, 'map')
    gbamix.set('class', 'Mix')
    SubElement(gbamix, 'operation').text = 'add'
    ba = SubElement(gbamix, 'a')
    g = SubElement(gbamix, 'b')
    bamix = SubElement(ba, 'map')
    bamix.set('class', 'Mix')
    SubElement(bamix, 'operation').text = 'add'
    b = SubElement(bamix, 'a')
    a = SubElement(bamix, 'b')
    return r, g, b, a

def addRGB(rgbmix):
    SubElement(rgbmix, 'operation').text = 'add'
    rgbmixa = SubElement(rgbmix, 'a')
    rgbmixb = SubElement(rgbmix, 'b')
    rgmix = SubElement(rgbmixa, 'map')
    rgmix.set('class', 'Mix')
    SubElement(rgmix, 'operation').text = 'add'
    rgmixa = SubElement(rgmix, 'a')
    rgmixb = SubElement(rgmix, 'b')
    return rgmixa, rgmixb, rgbmixb

def addClamp(channel):
    clamp = SubElement(channel, 'map')
    clamp.set('class', 'ToneMap')
    SubElement(clamp, 'clamp').text = 'true'
    return SubElement(clamp, 'child')

def addMultiply(node, r, g, b, alpha = None):
    mul = SubElement(node, 'map')
    mul.set('class', 'Mix')
    SubElement(mul, 'operation').text = 'mul'
    a = SubElement(mul, 'a')
    if alpha is None:
        SubElement(mul, 'b').text = "%.4f %.4f %.4f" % (r, g, b)
    else:
        SubElement(mul, 'b').text = "%.4f %.4f %.4f %4f" % (r, g, b, alpha)
    return a

def addChannel(node, r = None, g = None, b = None):
    channel = SubElement(node, 'map')
    channel.set('class', 'Channel')
    if r:
        SubElement(channel, 'rSource').text = r
    if g:
        SubElement(channel, 'gSource').text = g
    if b:
        SubElement(channel, 'bSource').text = b
    return SubElement(channel, 'child')

class CoronaHueNode(CoronaNode):
    '''Corona Hue Shift Node'''
    bl_idname = "CoronaHueNode"
    bl_label = "Hue Saturation Value"
    bl_icon = 'SMOOTH'

    hue = bpy.props.FloatProperty(name="Hue",
        soft_min = 0,
        soft_max = 1,
        default = 0.5,
        get=getHue,
        set=setHue)
    saturation = bpy.props.FloatProperty(name="Saturation",
        soft_min = -3,
        soft_max = 3,
        default = 1,
        get=getSat,
        set=setSat)
    value = bpy.props.FloatProperty(name="Value",
        soft_min = 0,
        soft_max = 1,
        default = 1,
        get=getVal,
        set=setVal)

    linear = bpy.props.BoolProperty(name = "Linear",
        default = True,
        get=getLinear,
        set=setLinear)

    matrix = bpy.props.FloatVectorProperty(name = "matrix", size = 16, default= (1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1))

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMap', "Color")
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'hue')
        layout.prop(self, 'saturation')
        layout.prop(self, 'value')
        layout.prop(self, 'linear')

    def get_node_params( self, root, inline = False, source = None):
        name_pointer = getNodePtr(self)
        rgbmix, rgbmix_def = getMap(root, name_pointer, 'Mix')
        if rgbmix != None:
            r,g,b = addRGB(rgbmix)

            rclamp = addClamp(r)
            gclamp = addClamp(g)
            bclamp = addClamp(b)

            rmix = SubElement(rclamp, 'map')
            rmix.set('class', 'Mix')
            m00, m01, m02 = addRGB(rmix)
            t00 = addChannel(addMultiply(m00, self.matrix[0], 0, 0), r = 'r')
            t01 = addChannel(addMultiply(m01, self.matrix[1], 0, 0), r = 'g')
            t02 = addChannel(addMultiply(m02, self.matrix[2], 0, 0), r = 'b')

            addChild(root, t00, self, "Color", inline)
            addChild(root, t01, self, "Color", inline)
            addChild(root, t02, self, "Color", inline)

            gmix = SubElement(gclamp, 'map')
            gmix.set('class', 'Mix')
            m10, m11, m12 = addRGB(gmix)
            t10 = addChannel(addMultiply(m10, 0, self.matrix[4], 0), g = 'r')
            t11 = addChannel(addMultiply(m11, 0, self.matrix[5], 0), g = 'g')
            t12 = addChannel(addMultiply(m12, 0, self.matrix[6], 0), g = 'b')

            addChild(root, t10, self, "Color", inline)
            addChild(root, t11, self, "Color", inline)
            addChild(root, t12, self, "Color", inline)

            bmix = SubElement(bclamp, 'map')
            bmix.set('class', 'Mix')
            m20, m21, m22 = addRGB(bmix)
            t20 = addChannel(addMultiply(m20, 0, 0, self.matrix[8]), b = 'r')
            t21 = addChannel(addMultiply(m21, 0, 0, self.matrix[9]), b = 'g')
            t22 = addChannel(addMultiply(m22, 0, 0, self.matrix[10]), b = 'b')

            addChild(root, t20, self, "Color", inline)
            addChild(root, t21, self, "Color", inline)
            addChild(root, t22, self, "Color", inline)

            if not inline: root.append(rgbmix_def)
        if inline:
            return rgbmix
        else:
            return getMapReference(name_pointer)


#--------------------------------
# Material output node.
#--------------------------------

def updateInputs(self, context):
    self.inputs["Diffuse Level"].hide = not self.use_diffuse or self.as_portal
    self.inputs["Diffuse Color"].hide = not self.use_diffuse or self.as_portal
    self.inputs["Translucency Level"].hide = not self.use_translucency or self.as_portal
    self.inputs["Translucency Color"].hide = not self.use_translucency or self.as_portal
    self.inputs["Reflect Level"].hide = not self.use_reflect or self.as_portal
    self.inputs["Reflect Color"].hide = not self.use_reflect or self.as_portal
    self.inputs["Reflect Gloss"].hide = not self.use_reflect or self.as_portal
    self.inputs["Fresnel"].hide = not self.use_reflect or self.as_portal
    self.inputs["Anisotropy"].hide = not self.use_reflect or self.as_portal
    self.inputs["Anisotropy Rotation"].hide = not self.use_reflect or self.as_portal
    self.inputs["Refract Level"].hide = not self.use_refract or self.as_portal
    self.inputs["Refract Color"].hide = not self.use_refract or self.as_portal
    self.inputs["Refract Gloss"].hide = not self.use_refract or self.as_portal
    self.inputs["Refract IOR"].hide = not self.use_refract or self.as_portal
    self.inputs["Refract Mode"].hide = not self.use_refract or self.as_portal
    self.inputs["Absorption Color"].hide = not self.use_absorption or self.as_portal
    self.inputs["Absorption Distance"].hide = not self.use_absorption or self.as_portal
    self.inputs["Scattering Color"].hide = not self.use_scattering or self.as_portal
    self.inputs["Scattering Direction"].hide = not self.use_scattering or self.as_portal
    self.inputs["Opacity"].hide = not self.use_opacity and not self.as_portal
    self.inputs["Normal"].hide = not self.use_bump or self.as_portal
    #self.inputs["Bump"].hide = not self.use_bump or self.as_portal
    self.inputs["Displacement"].hide = not self.use_displace or self.as_portal
    self.inputs["Emission Color"].hide = not self.use_emission or self.as_portal
    self.inputs["Directionality"].hide = not self.use_emission or self.as_portal
    self.inputs["Multiplier"].hide = not self.use_emission or self.as_portal
    self.inputs["Include"].hide = not self.use_emission or self.as_portal
    self.inputs["Exclude"].hide = not self.use_emission or self.as_portal
    return None

class CoronaMtlNode(CoronaNode):
    '''Corona Material Output Node'''
    bl_idname = "CoronaMtlNode"
    bl_label = "Corona Material"
    bl_icon = 'SMOOTH'

    params = []

    # as_portal       = CoronaMatProps.as_portal

    as_portal = bpy.props.BoolProperty( name = "Use material as light portal",
                                description = "Material will be used as a light portal. Portal material will not be visible to the camera",
                                default = False,
                                update = updateInputs)

    kelvin = CoronaMatProps.kelvin

    self_illumination = bpy.props.BoolProperty( name = "Self Illumination",
                                    description = "When true it disables sampling and provides self illumination only",
                                    default = False)

    both_sides = bpy.props.BoolProperty( name = "Two Sided",
                                description = "When true it emits light from both sides",
                                default = False)

    shadowcatcher_illuminator = bpy.props.BoolProperty( name = "Shadowcatcher Illuminator",
                                description = "",
                                default = False)

    disp_max = bpy.props.FloatProperty( name = "Max Displacement Value",
                                    default = 1.0,
                                    soft_min = 0.0,
                                    soft_max = 10.0)
    disp_min = bpy.props.FloatProperty( name = "Min Displacement Value",
                                    default = 0.0,
                                    soft_min = 0.0,
                                    soft_max = 10.0)
    disp_water = bpy.props.FloatProperty( name = "Displacement Water Level",
                                    default = 0.0,
                                    soft_min = 0.0,
                                    soft_max = 10.0)

    use_configure = bpy.props.BoolProperty(name = "Configure",
                                    description = "Configure the visible properties for this material (Connected nodes can't be hidden and the hidden values still take effect)",
                                    default = False)
    use_diffuse = bpy.props.BoolProperty(name = "Diffuse",
                                    description = "Show diffuse inputs",
                                    default = True,
                                    update=updateInputs)
    use_translucency = bpy.props.BoolProperty(name = "Translucency",
                                    description = "Show translucency inputs",
                                    default = True,
                                    update=updateInputs)
    use_reflect = bpy.props.BoolProperty(name = "Reflection",
                                    description = "Show reflection inputs",
                                    default = True,
                                    update=updateInputs)
    use_refract = bpy.props.BoolProperty(name = "Refract",
                                    description = "Show refraction inputs",
                                    default = True,
                                    update=updateInputs)
    use_absorption = bpy.props.BoolProperty(name = "Absorption",
                                    description = "Show absorption inputs",
                                    default = True,
                                    update=updateInputs)
    use_scattering = bpy.props.BoolProperty(name = "Scattering",
                                    description = "Show scattering inputs",
                                    default = True,
                                    update=updateInputs)
    use_bump = bpy.props.BoolProperty(name = "Normal",
                                    description = "Show normal input",
                                    default = True,
                                    update=updateInputs)
    use_displace = bpy.props.BoolProperty(name = "Displacement",
                                    description = "Show displacement input",
                                    default = False,
                                    update=updateInputs)
    use_opacity = bpy.props.BoolProperty(name = "Opacity",
                                    description = "Show opacity input",
                                    default = True,
                                    update=updateInputs)
    use_emission = bpy.props.BoolProperty(name = "Emit",
                                    description = "Show emission inputs",
                                    default = True,
                                    update=updateInputs)

    def init( self, context):
        self.inputs.new( 'CoronaDiffuseLevel', "Diffuse Level")
        self.inputs.new( 'CoronaKd', "Diffuse Color")
        self.inputs.new( 'CoronaTranslucencyLevel', "Translucency Level")
        self.inputs.new( 'CoronaTranslucency', "Translucency Color")
        self.inputs.new( 'CoronaReflectLevel', "Reflect Level")
        self.inputs.new( 'CoronaKs', "Reflect Color")
        self.inputs.new( 'CoronaReflectGloss', "Reflect Gloss")
        self.inputs.new( 'CoronaReflectFresnel', "Fresnel")
        self.inputs.new( 'CoronaAnisotropy', "Anisotropy")
        self.inputs.new( 'CoronaAnisotropyRot', "Anisotropy Rotation")
        self.inputs.new( 'CoronaRefractLevel', "Refract Level")
        self.inputs.new( 'CoronaRefractColor', "Refract Color")
        self.inputs.new( 'CoronaRefractGloss', "Refract Gloss")
        self.inputs.new( 'CoronaRefractIOR', "Refract IOR")
        self.inputs.new( 'CoronaRefractMode', "Refract Mode")
        self.inputs.new( 'CoronaAbsorptionColor', "Absorption Color")
        self.inputs.new( 'CoronaAbsorptionDist', "Absorption Distance")
        self.inputs.new( 'CoronaScattering', "Scattering Color")
        self.inputs.new( 'CoronaMeanCosine', "Scattering Direction")
        self.inputs.new( 'CoronaOpacity', "Opacity")
        # self.inputs.new( 'CoronaNormal', "Normal")

        #depreciated
        self.inputs.new( 'CoronaBump', "Bump")
        self.inputs['Bump'].hide = True

        self.inputs.new( 'CoronaBump', "Displacement")
        self.inputs.new( 'CoronaKe', "Emission Color")
        self.inputs.new( 'CoronaEmissionGloss', "Directionality")
        self.inputs.new( 'CoronaEmissionMultZero', "Multiplier")
        self.inputs.new( 'CoronaNormalMap', "Normal")
        self.inputs.new( 'CoronaObjectList', "Include")
        self.inputs.new( 'CoronaObjectList', "Exclude")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):

        layout.prop( self, "use_configure")
        if self.use_configure:
            layout.prop( self, "as_portal")

        if self.use_configure and not self.as_portal:
            row = layout.row()
            row.prop( self, "use_diffuse")
            row.prop( self, "use_translucency")
            row.prop( self, "use_reflect")
            row = layout.row()
            row.prop( self, "use_refract")
            row.prop( self, "use_absorption")
            row.prop( self, "use_scattering")
            row = layout.row()
            row.prop( self, "use_opacity")
            row.prop( self, "use_bump")
            row.prop( self, "use_displace")
            row = layout.row()
            row.prop( self, "use_emission")
        if self.use_emission and not self.as_portal:
            layout.prop( self, "self_illumination")
            layout.prop( self, "kelvin", slider=True)
        if self.use_displace and not self.as_portal:
            layout.prop( self, "disp_min")
            layout.prop( self, "disp_max")
            layout.prop( self, "disp_water")

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Material node's parameters as a string '''
        # Add material definition
        try:
            name_pointer = getNodePtr(self)

            xml_mat, xml_mat_def = getMat(root, name_pointer)

            if xml_mat != None:

                if not self.as_portal:
                    if self.use_diffuse or self.some_linked("Diffuse Color"):
                        addSocket(root, xml_mat, self, "Diffuse Color", "diffuse", inline)

                    if self.use_translucency or self.some_linked("Translucency Level", "Translucency Color"):
                        xml_trans = SubElement(xml_mat, 'translucency')
                        addSocket(root, xml_trans, self, "Translucency Level", "level", inline)
                        addSocket(root, xml_trans, self, "Translucency Color", "color", inline)


                    if self.use_reflect or self.some_linked("Reflect Color", "Reflect Gloss",
                        "Fresnel", "Anisotropy", "Anisotropy Rotation"):
                        xml_reflect = SubElement(xml_mat, 'reflect')
                        addSocket(root, xml_reflect, self, "Reflect Color", "color", inline)
                        addSocket(root, xml_reflect, self, "Reflect Gloss", "glossiness", inline)
                        addSocket(root, xml_reflect, self, "Fresnel", "ior", inline)
                        addSocket(root, xml_reflect, self, "Anisotropy", "anisotropy", inline)
                        addSocket(root, xml_reflect, self, "Anisotropy Rotation", "anisoRotation", inline)
                        # TODO add extra anisotropy parameters


                    if self.use_refract or self.some_linked("Refract Color", "Refract IOR",
                        "Refract Gloss", "Refract Mode"):
                        xml_refract = SubElement(xml_mat, 'refract')
                        addSocket(root, xml_refract, self, "Refract Color", "color", inline)
                        addSocket(root, xml_refract, self, "Refract IOR", "ior", inline)
                        addSocket(root, xml_refract, self, "Refract Gloss", "glossiness", inline)
                        addSocket(root, xml_refract, self, "Refract Mode", "glassMode", inline)


                    if self.use_absorption or self.use_scattering or self.some_linked("Absorption Color", "Absorption Distance",
                        "Scattering Color", "Scattering Direction"):
                        xml_vol = SubElement(xml_mat, 'volume')
                        if self.use_absorption or self.some_linked("Absorption Color", "Absorption Distance"):
                            addSocket(root, xml_vol, self, "Absorption Color", "attenuationColor", inline)
                            addSocket(root, xml_vol, self, "Absorption Distance", "attenuationDist", inline)
                        if self.use_scattering or self.some_linked("Scattering Color", "Scattering Direction"):
                            addSocket(root, xml_vol, self, "Scattering Color", "scatteringAlbedo", inline)
                            addSocket(root, xml_vol, self, "Scattering Direction", "meanCosine", inline)


                    #compatibility
                    try:
                        if self.some_linked("Bump"):
                            addSocket(root, xml_mat, self, "Bump", "bump", inline)
                    except:
                        pass
                    try:
                        if self.some_linked("Normal"):
                            addSocket(root, xml_mat, self, "Normal", "bump", inline)
                    except:
                        pass


                    if "Displacement" in self.inputs and self.inputs["Displacement"].is_linked:
                        xml_disp = SubElement(xml_mat, 'displacement')
                        xml_map = addSocket(root, xml_disp, self, "Displacement", "map", True)
                        xml_map.set('class', 'Displacement')
                        # xml_map = SubElement(xml_disp, 'map').text = '0.5 0.5 0.5'
                        # addSocket(root, xml_map, self, "Displacement", inline)
                        SubElement(xml_disp, 'max').text = '%.3f' % self.disp_max
                        SubElement(xml_disp, 'min').text = '%.3f' % self.disp_min
                        SubElement(xml_disp, 'waterLevel').text = '%.3f' % self.disp_water


                    if self.use_opacity or self.some_linked("Opacity"):
                        addSocket(root, xml_mat, self, "Opacity", "opacity", inline)
                    else:
                        if self.use_diffuse or self.some_linked("Diffuse Color"):
                            params = self.inputs["Diffuse Color"].get_alpha_socket_params(self, root, inline)
                            if params is not None:
                                xml = SubElement(xml_mat, "opacity")
                                xml.append(params)

                    if (self.use_emission or self.some_linked("Emission Color")) and self.inputs.find("Emission Color") != -1:
                        xml_emit = SubElement(xml_mat, 'emission')
                        addSocket(root, xml_emit, self, "Emission Color", "color", inline)
                        addSocket(root, xml_emit, self, "Directionality", "glossiness", inline)
                        SubElement(xml_emit, 'disableSampling').text = 'True' if self.self_illumination else 'False'
                        SubElement(xml_emit, 'twosided').text = 'True' if self.both_sides else 'False'
                        SubElement(xml_emit, 'shadowcatcherIlluminator').text = 'True' if self.shadowcatcher_illuminator else 'False'

                    if self.inputs.find("Include") != -1 and (self.inputs["Include"].is_linked or self.inputs["Exclude"].is_linked):
                        xml_inex = SubElement(xml_emit, 'includeExclude')
                        if self.inputs["Include"].is_linked:
                            includes = self.inputs["Include"].get_socket_params(self, root, inline)
                            for inc in includes:
                                SubElement(xml_inex, 'included').text = inc.name

                        if self.inputs["Exclude"].is_linked:
                            excludes = self.inputs["Exclude"].get_socket_params(self, root, inline)
                            for excl in excludes:
                                SubElement(xml_inex, 'excluded').text = excl.name
                else: # as_portal
                    addSocket(root, xml_mat, self, "Opacity", "opacity", inline)
                    SubElement(xml_mat, 'portal').text = 'true' if self.as_portal else 'false'

                if not inline: root.append(xml_mat_def)


            # Return material reference
            if inline:
                return xml_mat
            else:
                return getReference(name_pointer)
        except Exception as e:
            print_exception(e)

#--------------------------------
# Light material output node.
#--------------------------------
class CoronaLightMtlNode(CoronaNode):
    '''Corona Light Material Output Node'''
    bl_idname = "CoronaLightMtlNode"
    bl_label = "Corona Light Material"
    bl_icon = 'SMOOTH'

    kelvin          = CoronaMatProps.kelvin
    ies_profile     = CoronaMatProps.ies_profile
    keep_sharp      = CoronaMatProps.keep_sharp
    ies_translate   = CoronaMatProps.ies_translate
    ies_rotate      = CoronaMatProps.ies_rotate
    ies_scale       = CoronaMatProps.ies_scale
    both_sides       = CoronaMatProps.both_sides
    shadowcatcher_illuminator       = CoronaMatProps.shadowcatcher_illuminator

    self_illumination = bpy.props.BoolProperty( name = "Self Illumination",
                                    description = "When true it disables sampling and provides self illumination only",
                                    default = False)

    def init( self, context):
        self.inputs.new( 'CoronaKe', "Emission Color")
        self.inputs.new( 'CoronaEmissionGloss', "Directionality")
        self.inputs.new( 'CoronaEmissionMult', "Multiplier")
        self.inputs.new( 'CoronaOpacity', "Opacity")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):
        layout.prop( self, "kelvin", slider=True)
        layout.prop( self, "ies_profile")
        layout.prop( self, "keep_sharp")
        # layout.prop( self, "ies_translate")
        layout.prop( self, "ies_rotate")
        # layout.prop( self, "ies_scale")
        layout.prop( self, "self_illumination")
        layout.prop( self, "both_sides")
        layout.prop( self, "shadowcatcher_illuminator")

    def get_node_params( self, root, inline = False, source = None):
        # Add material definition
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer)

        if xml_mat != None:
            xml_emit = SubElement(xml_mat, 'emission')

            addSocket(root, xml_emit, self, "Emission Color", "color", inline)
            addSocket(root, xml_emit, self, "Directionality", "glossiness", inline)
            SubElement(xml_emit, 'disableSampling').text = 'True' if self.self_illumination else 'False'
            SubElement(xml_emit, 'twosided').text = 'True' if self.both_sides else 'False'
            SubElement(xml_emit, 'shadowcatcherIlluminator').text = 'True' if self.shadowcatcher_illuminator else 'False'

            if self.ies_profile != '':

                mat = (mathutils.Matrix.Translation(self.ies_translate)
                    * mathutils.Matrix.Rotation((self.ies_rotate[0]), 4, 'X')
                    * mathutils.Matrix.Rotation((self.ies_rotate[1]), 4, 'Y')
                    * mathutils.Matrix.Rotation((self.ies_rotate[2]), 4, 'Z')
                    * mathutils.Matrix.Scale(self.ies_scale[0], 4, (1.0, 0.0, 0.0))
                    * mathutils.Matrix.Scale(self.ies_scale[1], 4, (0.0, 1.0, 0.0))
                    * mathutils.Matrix.Scale(self.ies_scale[2], 4, (0.0, 0.0, 1.0))
                    )

                xml_ies = SubElement(xml_emit, 'ies')
                SubElement(xml_ies, 'file').text = realpath( self.ies_profile)
                SubElement(xml_ies, 'tm').text = getTransform(mat)
                SubElement(xml_ies, 'sharpnessFake').text = 'true' if self.keep_sharp else 'false'

            addSocket(root, xml_mat, self, "Opacity", "opacity", inline)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)


#--------------------------------
# Volume material output node.
#--------------------------------
class CoronaVolumeMtlNode(CoronaNode):
    '''Corona Volume Material Output Node'''
    bl_idname = "CoronaVolumeMtlNode"
    bl_label = "Corona Volume Material"
    bl_icon = 'SMOOTH'

    params = []

    kelvin = CoronaMatProps.kelvin

    def init( self, context):
        self.inputs.new( 'CoronaKe', "Emission Color")
        self.inputs.new( 'CoronaEmissionGloss', "Directionality")
        self.inputs.new( 'CoronaEmissionMult', "Multiplier")
        self.inputs.new( 'CoronaAbsorptionColor', "Absorption Color")
        self.inputs.new( 'CoronaAbsorptionDist', "Absorption Distance")
        self.inputs.new( 'CoronaScattering', "Scattering Color")
        self.inputs.new( 'CoronaMeanCosine', "Scattering Direction")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):
        layout.prop( self, "kelvin", slider=True)

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Material node's parameters as a string '''

        # Add material definition
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer)

        if xml_mat != None:
            xml_vol = SubElement(xml_mat, 'volume')

            addSocket(root, xml_vol, self, "Absorption Color", "attenuationColor", inline)
            addSocket(root, xml_vol, self, "Absorption Distance", "attenuationDist", inline)
            addSocket(root, xml_vol, self, "Emission Color", "emissionColor", inline)
            addSocket(root, xml_vol, self, "Directionality", "emissionDist", inline)
            addSocket(root, xml_vol, self, "Scattering Color", "scatteringAlbedo", inline)
            addSocket(root, xml_vol, self, "Scattering Direction", "meanCosine", inline)

            # xml_refract = SubElement(xml_mat, 'refract')
            # SubElement(xml_refract, 'glassMode').text = 'caustics'
            SubElement(xml_mat, 'opacity').text = '0 0 0'
            if not inline: root.append(xml_mat_def)


        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)


#--------------------------------
# Hair material node.
#--------------------------------


def updateHairInputs(self, context):
    self.inputs["Tint"].hide = not self.use_hair_color

class CoronaHairMtlNode(CoronaNode):
    '''Corona Hair Material Node'''
    bl_idname = "CoronaHairMtlNode"
    bl_label = "Corona Hair Material"
    bl_icon = 'SMOOTH'

    params = []

    use_hair_color = bpy.props.BoolProperty( name = "Use Dye",
                                        description = "When enabled allows control of hair dye",
                                        default = False,
                                        update = updateHairInputs)

    randomizeMelanin = bpy.props.FloatProperty( name = "Randomize Melanin",
                                     description = "Controls the randomization of pigment amount in hair. Value of 0 leads to no randomization, value of 1 leads to completely random amount of hair pigment for each hair strand.",
                                     default = 0.1,
                                     min = 0,
                                     max = 1)

    def init( self, context):
        self.inputs.new( 'CoronaDyeColor', "Tint").hide = True
        self.inputs.new( 'CoronaMelanin', "Melanin")
        self.inputs.new( 'CoronaPheomelanin', "Pheomelanin")
        self.inputs.new( 'CoronaColorlessSpecular', "Colorless Specular")
        self.inputs.new( 'CoronaColoredSpecular', "Colored Specular")
        self.inputs.new( 'CoronaHairTransmission', "Transmission")
        self.inputs.new( 'CoronaHairGlossiness', "Glossiness")
        self.inputs.new( 'CoronaColorlessGlossiness', "Colorless Glossiness")
        self.inputs.new( 'CoronaSoftness', "Softness")
        self.inputs.new( 'CoronaHighlightShift', "Highlight Shift")
        self.inputs.new( 'CoronaHairIor', "IOR")
        self.inputs.new( 'CoronaGlintStrength', "Glint Strength")
        self.inputs.new( 'CoronaHairDiffuse', "Diffuse Color")
        self.inputs.new( 'CoronaHairOpacity', "Opacity")
        self.inputs.new( 'CoronaLinked', "Bump", "Bump mapping")

        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):
        layout.prop( self, "use_hair_color")
        layout.prop( self, "randomizeMelanin", slider=True)

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Material node's parameters as a string '''

        # Add material definition
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer, 'Hair')

        if xml_mat != None:
            if self.use_hair_color:
                addSocket(root, xml_mat, self, "Tint", "tint", inline)
            addSocket(root, xml_mat, self, "Melanin", "melanin", inline)
            addSocket(root, xml_mat, self, "Pheomelanin", "pheomelanin", inline)
            SubElement(xml_mat, 'randomizeMelanin').text = '%.6f' % self.randomizeMelanin
            addSocket(root, xml_mat, self, "Colorless Specular", "colorlessSpecular", inline)
            addSocket(root, xml_mat, self, "Colored Specular", "coloredSpecular", inline)
            addSocket(root, xml_mat, self, "Transmission", "transmission", inline)
            addSocket(root, xml_mat, self, "Glossiness", "glossiness", inline)
            addSocket(root, xml_mat, self, "Colorless Glossiness", "colorlessGlossiness", inline)
            addSocket(root, xml_mat, self, "Softness", "softness", inline)
            addSocket(root, xml_mat, self, "Highlight Shift", "highlightShift", inline)
            addSocket(root, xml_mat, self, "IOR", "ior", inline)
            addSocket(root, xml_mat, self, "Glint Strength", "glintStrength", inline)
            addSocket(root, xml_mat, self, "Diffuse Color", "diffuseColor", inline)
            addSocket(root, xml_mat, self, "Opacity", "opacity", inline)
            addSocket(root, xml_mat, self, "Bump", "bump", inline)

            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)


#--------------------------------
# Skin material node.
#--------------------------------


def updateSubsurfaceLayers(self, context):
    old_num_subsurfaces = math.floor(len([i for i in self.inputs if i.name.startswith('Subsurface ')])/3)
    if old_num_subsurfaces < self.num_subsurfaces:
        for x in range(old_num_subsurfaces, self.num_subsurfaces):
            self.inputs.new( 'CoronaColor', "Subsurface Scatter Color %d" % (x+1))
            self.inputs.new( 'CoronaLayeredWeight', "Subsurface Radius %d" % (x+1))
            self.inputs.new( 'CoronaWeight', "Subsurface Weight %d" % (x+1))
        old_num_subsurfaces = self.num_subsurfaces
    if old_num_subsurfaces > self.num_subsurfaces:
        for x in range(old_num_subsurfaces, self.num_subsurfaces, -1):
            self.inputs.remove( self.inputs["Subsurface Scatter Color %d" % x])
            self.inputs.remove( self.inputs["Subsurface Radius %d" % x])
            self.inputs.remove( self.inputs["Subsurface Weight %d" % x])
        old_num_subsurfaces = self.num_subsurfaces
    return None

def updateReflectionLayers(self, context):
    old_num_reflections = math.floor(len([i for i in self.inputs if i.name.startswith('Reflection ')])/3)
    if old_num_reflections < self.num_reflections:
        for x in range(old_num_reflections, self.num_reflections):
            self.inputs.new( 'CoronaColor', "Reflection Color %d" % (x+1))
            self.inputs.new( 'CoronaHairIor', "Reflection IOR %d" % (x+1))
            self.inputs.new( 'CoronaHairGlossiness', "Reflection Glossiness %d" % (x+1))
        old_num_reflections = self.num_reflections
    if old_num_reflections > self.num_reflections:
        for x in range(old_num_reflections, self.num_reflections, -1):
            self.inputs.remove( self.inputs["Reflection Color %d" % x])
            self.inputs.remove( self.inputs["Reflection IOR %d" % x])
            self.inputs.remove( self.inputs["Reflection Glossiness %d" % x])
        old_num_reflections = self.num_reflections
    return None

class CoronaSkinMtlNode(CoronaNode):
    '''Corona Skin Material Node'''
    bl_idname = "CoronaSkinMtlNode"
    bl_label = "Corona Skin Material"
    bl_icon = 'SMOOTH'

    params = []

    sssRadiusScale = bpy.props.FloatProperty( name = "SSS Radius Scale",
                                     description = "Controls the size of the subsurface scattering of all layers. Serves as a multiplier of each layer radius.",
                                     default = 1,
                                     min = 0,
                                     soft_max = 10)

    sssMode = bpy.props.EnumProperty( name = "SSS Mode",
                                     description = "Controls the size of the subsurface scattering of all layers. Serves as a multiplier of each layer radius.",
                                     default='Directional',
                                     items=[
                                        ('Directional', 'Directional', ''),
                                        ('Diffusion', 'Diffusion', ''),
                                     ])

    num_subsurfaces = bpy.props.IntProperty(name = 'Subsurfaces', default=0, min=0, max=3, update=updateSubsurfaceLayers)
    num_reflections = bpy.props.IntProperty(name = 'Reflections', default=0, min=0, max=2, update=updateReflectionLayers)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Skin Color")
        self.inputs.new( 'CoronaHairOpacity', "Opacity")
        self.inputs.new( 'CoronaLinked', "Bump", "Bump mapping")

        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):
        layout.prop( self, "sssRadiusScale")
        layout.prop( self, "sssMode")
        layout.prop( self, "num_subsurfaces")
        layout.prop( self, "num_reflections")

    def get_node_params( self, root, inline = False, source = None):
        ''' Return the Material node's parameters as a string '''

        # Add material definition
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer, 'Skin')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Skin Color", "skinColor", inline)
            SubElement(xml_mat, 'sssRadiusScale').text = '%.6f' % self.sssRadiusScale
            SubElement(xml_mat, 'sssMode').text = self.sssMode

            addSocket(root, xml_mat, self, "Opacity", "opacity", inline)
            addSocket(root, xml_mat, self, "Bump", "bump", inline)


            for x in range(1, self.num_subsurfaces+1, 1):
                xml_subsurface = SubElement(xml_mat, 'subsurface')
                SubElement(xml_subsurface, 'index').text = "%d" % x
                addSocket(root, xml_subsurface, self, "Subsurface Scatter Color %d" % x, "scatterColor", inline)
                addSocket(root, xml_subsurface, self, "Subsurface Radius %d" % x, "radius", inline)
                addSocket(root, xml_subsurface, self, "Subsurface Weight %d" % x, "weight", inline)


            for x in range(1, self.num_reflections+1, 1):
                xml_reflection = SubElement(xml_mat, 'reflection')
                SubElement(xml_reflection, 'index').text = "%d" % x
                addSocket(root, xml_reflection, self, "Reflection Color %d" % x, "color", inline)
                addSocket(root, xml_reflection, self, "Reflection IOR %d" % x, "ior", inline)
                addSocket(root, xml_reflection, self, "Reflection Glossiness %d" % x, "glossiness", inline)

            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)
#--------------------------------
# Node category for extending the Add menu, toolbar panels
#   and search operator
# Base class for node categories
#--------------------------------
class CoronaNodeCategory( NodeCategory):

    # def copy(self, cat):
    #     pass

    # @classmethod
    # def poll(cls, context):
    #     renderer = context.scene.render.engine
    #     if hasattr(context, "bl_idname"):
    #         return renderer == 'CORONA' and (context.space_data.tree_type == 'ShaderNodeTree' or context.space_data.tree_type == "CoronaNodeTree")
    #     else:
    #         return renderer == 'CORONA'
    @classmethod
    def poll(cls, context):
        return context.space_data.tree_type == 'ShaderNodeTree'

helperMaterialName = "Helper Material for Corona Nodes"

def getHelperMaterial():
    helperMaterial = bpy.data.materials.get(helperMaterialName)
    if helperMaterial is None:
        helperMaterial = newHelperMaterial()
    return helperMaterial

def newHelperMaterial():
    material = bpy.data.materials.new(helperMaterialName)
    material.use_nodes = True
    material.use_fake_user = True
    return material

class CurveMapPointCache(bpy.types.PropertyGroup):
    handle_type = bpy.props.StringProperty()
    location = bpy.props.FloatVectorProperty(size = 2)

class CurveMapCache(bpy.types.PropertyGroup):
    extend = bpy.props.StringProperty()
    points = bpy.props.CollectionProperty(type = CurveMapPointCache)
    dirty = bpy.props.BoolProperty(default = True)

# __NODES = []

class CoronaCurveNode2(CoronaNode):
    bl_idname = "CoronaCurveNode2"
    bl_label = "Curve node UI"
    bl_width_default = 200

    curveMapCache = bpy.props.PointerProperty(type = CurveMapCache)
    num_items = bpy.props.IntProperty(name = 'Number of points', default=10, min=2, soft_max=20)
    curveid = bpy.props.StringProperty(name = 'Curve ID')

    def init(self, context):
        self.inputs.new( 'CoronaColor', "Input")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new("CoronaColor", "Curve")
        self.curveid = 'Curve %d' % current_milli_time()
        debug("Curve", self.curveid)
        self.createCurveNode()

    def draw_buttons(self, context, layout):
        layout.template_curve_mapping(self.curveNode, "mapping", type = "NONE")
        layout.prop(self, "num_items", "Exported Curve Resolution")

    def draw_label( self):
        return self.bl_label

    def get_node_params( self, root, inline = False, source = None):
    # def execute(self):
        # load cached curve map if available
        # this happens when the node tree is appended to another file
        if not self.curveMapCache.dirty:
            self.loadCachedCurveMap()
            self.curveMapCache.dirty = True

        mapping = self.mapping
        curve = mapping.curves[3]
        try: curve.evaluate(0.5)
        except: mapping.initialize()
        # return curve.evaluate

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Curve')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Input", "child", inline)
            for x in range(1, self.num_items+2):
                point = SubElement(xml_mat, 'point')
                pos = (x - 1) / self.num_items
                point.set('position', '%.6f' % pos)
                point.text = '%.6f' % curve.evaluate(pos)
                # xml_mat.append(self.inputs["Point %d" % x].get_socket_value(self, root))
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

    def createCurveNode(self):
        material = getHelperMaterial()
        node = material.node_tree.nodes.new("ShaderNodeRGBCurve")
        node.name = self.curveid
        mapping = self.mapping
        mapping.use_clip = True
        mapping.clip_min_y = -0.5
        mapping.clip_max_y = 1.5
        self.resetEndPoints()
        return node

    def removeCurveNode(self):

        material = getHelperMaterial()
        tree = material.node_tree
        curveNode = tree.nodes.get(self.curveid)
        if curveNode is not None:
            tree.nodes.remove(curveNode)

    def resetEndPoints(self):
        points = self.curve.points
        points[0].location = (0, 0)
        points[-1].location = (1, 1)
        self.mapping.update()

    def duplicate(self, sourceNode):
        self.createCurveNode()
        self.copyOtherCurve(sourceNode.curve)

    def delete(self):
        self.removeCurveNode()

    def cacheCurveMap(self):
        curve = self.curve
        self.curveMapCache.extend = curve.extend
        self.curveMapCache.points.clear()
        for point in curve.points:
            item = self.curveMapCache.points.add()
            item.handle_type = point.handle_type
            item.location = point.location
        self.curveMapCache.dirty = False

    def loadCachedCurveMap(self):
        self.copyOtherCurve(self.curveMapCache)

    def copyOtherCurve(self, otherCurve):
        curve = self.curve
        curve.extend = otherCurve.extend
        curvePoints = curve.points
        for i, point in enumerate(otherCurve.points):
            if len(curvePoints) == i:
                curvePoints.new(50, 50) # random start position
            curvePoints[i].location = point.location
            curvePoints[i].handle_type = point.handle_type
        self.mapping.update()

    @property
    def curve(self):
        return self.mapping.curves[3]

    @property
    def mapping(self):
        return self.curveNode.mapping

    @property
    def curveNode(self):
        material = getHelperMaterial()
        node = None
        try:
            node = material.node_tree.nodes[self.curveid]
        except:
            pass
        if node is None:
            node = self.createCurveNode()
        return node

class CoronaRGBCurveNode(CoronaNode):
    bl_idname = "CoronaRGBCurveNode"
    bl_label = "RGB Curve"
    bl_width_default = 200

    curveMapCache = bpy.props.PointerProperty(type = CurveMapCache)
    num_items = bpy.props.IntProperty(name = 'Number of points', default=10, min=2, soft_max=20)
    curveid = bpy.props.StringProperty(name = 'Curve ID')

    def init(self, context):
        self.inputs.new( 'CoronaColor', "Input")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new("NodeSocketShader", "Curve")
        self.curveid = 'Curve %d' % current_milli_time()
        debug("Curve", self.curveid)
        self.createCurveNode()

    def draw_buttons(self, context, layout):
        layout.template_curve_mapping(self.curveNode, "mapping", type = "COLOR")
        layout.prop(self, "num_items", "Exported Curve Resolution")

    def draw_label( self):
        return self.bl_label

    def addPoints(self, node, curve, num):
        for x in range(1, num+2):
            point = SubElement(node, 'point')
            pos = (x - 1) / num
            point.set('position', '%.6f' % pos)
            point.text = '%.6f' % curve.evaluate(pos)

    def addCurve(self, root, node, curve, num):
        curveNode = SubElement(node, 'map')
        curveNode.set('class', 'Curve')
        self.addPoints(curveNode, curve, num)
        addChild(root, node, self, "UV Map", True)
        return SubElement(curveNode, "child")

    def get_node_params( self, root, inline = False, source = None):
        # load cached curve map if available
        # this happens when the node tree is appended to another file
        if not self.curveMapCache.dirty:
            self.loadCachedCurveMap()
            self.curveMapCache.dirty = True

        mapping = self.mapping
        curve = mapping.curves[0]
        try: curve.evaluate(0.5)
        except: mapping.initialize()
        curve = mapping.curves[1]
        try: curve.evaluate(0.5)
        except: mapping.initialize()
        curve = mapping.curves[2]
        try: curve.evaluate(0.5)
        except: mapping.initialize()
        curve = mapping.curves[3]
        try: curve.evaluate(0.5)
        except: mapping.initialize()

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Curve')

        if xml_mat != None:
            self.addPoints(xml_mat, curve, self.num_items)
            finalcurvein = SubElement(xml_mat, 'child')
            addChild(root, xml_mat, self, "UV Map", True)
            mixrgb = SubElement(finalcurvein, 'map')
            mixrgb.set('class', 'Mix')
            r,g,b = addRGB(mixrgb)

            rmul = addMultiply(r, 1, 0, 0)
            gmul = addMultiply(g, 0, 1, 0)
            bmul = addMultiply(b, 0, 0, 1)
            rinput = self.addCurve(root, rmul, mapping.curves[0], self.num_items)
            ginput = self.addCurve(root, gmul, mapping.curves[1], self.num_items)
            binput = self.addCurve(root, bmul, mapping.curves[2], self.num_items)

            addChild(root, rinput, self, "Input", inline)
            addChild(root, ginput, self, "Input", inline)
            addChild(root, binput, self, "Input", inline)

            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

    def createCurveNode(self):
        material = getHelperMaterial()
        node = material.node_tree.nodes.new("ShaderNodeRGBCurve")
        node.name = self.curveid
        mapping = self.mapping
        mapping.use_clip = True
        mapping.clip_min_y = -0.5
        mapping.clip_max_y = 1.5
        self.resetEndPoints()
        return node

    def removeCurveNode(self):

        material = getHelperMaterial()
        tree = material.node_tree
        curveNode = tree.nodes.get(self.curveid)
        if curveNode is not None:
            tree.nodes.remove(curveNode)

    def resetEndPoints(self):
        points = self.curve.points
        points[0].location = (0, 0)
        points[-1].location = (1, 1)
        self.mapping.update()

    def duplicate(self, sourceNode):
        self.createCurveNode()
        self.copyOtherCurve(sourceNode.curve)

    def delete(self):
        self.removeCurveNode()

    def cacheCurveMap(self):
        curve = self.curve
        self.curveMapCache.extend = curve.extend
        self.curveMapCache.points.clear()
        for point in curve.points:
            item = self.curveMapCache.points.add()
            item.handle_type = point.handle_type
            item.location = point.location
        self.curveMapCache.dirty = False

    def loadCachedCurveMap(self):
        self.copyOtherCurve(self.curveMapCache)

    def copyOtherCurve(self, otherCurve):
        curve = self.curve
        curve.extend = otherCurve.extend
        curvePoints = curve.points
        for i, point in enumerate(otherCurve.points):
            if len(curvePoints) == i:
                curvePoints.new(50, 50) # random start position
            curvePoints[i].location = point.location
            curvePoints[i].handle_type = point.handle_type
        self.mapping.update()

    @property
    def curve(self):
        return self.mapping.curves[3]

    @property
    def mapping(self):
        return self.curveNode.mapping

    @property
    def curveNode(self):
        material = getHelperMaterial()
        node = None
        try:
            node = material.node_tree.nodes[self.curveid]
        except:
            pass
        if node is None:
            node = self.createCurveNode()
        return node



class ColorRampMapPointCache(bpy.types.PropertyGroup):
    handle_type = bpy.props.StringProperty()
    location = bpy.props.FloatVectorProperty(size = 2)

class ColorRampMapCache(bpy.types.PropertyGroup):
    extend = bpy.props.StringProperty()
    points = bpy.props.CollectionProperty(type = ColorRampMapPointCache)
    dirty = bpy.props.BoolProperty(default = True)

class CoronaColorRampNode(CoronaNode):
    bl_idname = "CoronaColorRampNode"
    bl_label = "Color Ramp"
    bl_width_default = 250

    colorRampMapCache = bpy.props.PointerProperty(type = ColorRampMapCache)
    num_items = bpy.props.IntProperty(name = 'Number of points', default=10, min=2, soft_max=20)
    colorRampid = bpy.props.StringProperty(name = 'Color Ramp ID')

    def init(self, context):
        self.inputs.new( 'CoronaColor', "Input")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new("CoronaColor", "ColorRamp")
        self.colorRampid = 'ColorRamp %d' % current_milli_time()
        self.createRampNode()

    def draw_buttons(self, context, layout):
        layout.template_color_ramp(self.colorRampNode, "color_ramp", expand=True)
        layout.prop(self, "num_items", "Exported ColorRamp Resolution")

    def draw_label( self):
        return self.bl_label

    def get_node_params( self, root, inline = False, source = None):
    # def execute(self):
        # load cached colorRamp map if available
        # this happens when the node tree is appended to another file
        if not self.colorRampMapCache.dirty:
            self.loadCachedColorRampMap()
            self.colorRampMapCache.dirty = True

        colorRamp = self.color_ramp

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Interpolation')
        if xml_mat != None:
            addSocket(root, xml_mat, self, "Input", "child", inline)
            for x in range(1, self.num_items+2):
                point = SubElement(xml_mat, 'point')
                pos = (x - 1) / self.num_items
                point.set('position', '%.6f' % pos)
                value = colorRamp.evaluate(pos)
                point.text = '%.6f %.6f %.6f %.6f' % (value[0], value[1], value[2], value[3])
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

    def createRampNode(self):
        material = getHelperMaterial()
        node = material.node_tree.nodes.new("ShaderNodeValToRGB")
        node.name = self.colorRampid
        color_ramp = self.color_ramp
        color_ramp.color_mode = 'RGB'
        color_ramp.hue_interpolation = 'NEAR'
        color_ramp.interpolation = 'LINEAR'
        self.resetEndPoints()
        return node

    def removeColorRampNode(self):

        material = getHelperMaterial()
        tree = material.node_tree
        colorRampNode = tree.nodes.get(self.colorRampid)
        if colorRampNode is not None:
            tree.nodes.remove(colorRampNode)

    def resetEndPoints(self):
        points = self.color_ramp.elements
        for i in range(len(points) - 1, 2, -1):
            points.remove(points[i])
        self.color_ramp.update()

    def duplicate(self, sourceNode):
        self.createRampNode()
        self.copyOtherColorRamp(sourceNode.colorRamp)

    def delete(self):
        self.removeColorRampNode()

    def cacheColorRampMap(self):
        color_ramp = self.color_ramp
        self.colorRampMapCache.extend = colorRamp.extend
        self.colorRampMapCache.points.clear()
        for point in colorRamp.points:
            item = self.colorRampMapCache.points.add()
            item.handle_type = point.handle_type
            item.location = point.location
        self.colorRampMapCache.dirty = False

    def loadCachedColorRampMap(self):
        self.copyOtherColorRamp(self.colorRampMapCache)

    def copyOtherColorRamp(self, otherColorRamp):
        colorRamp = self.color_ramp
        colorRamp.extend = otherColorRamp.extend
        colorRampPoints = colorRamp.points
        for i, point in enumerate(otherColorRamp.points):
            if len(colorRampPoints) == i:
                colorRampPoints.new(50, 50) # random start position
            colorRampPoints[i].location = point.location
            colorRampPoints[i].handle_type = point.handle_type
        self.color_ramp.update()

    @property
    def color_ramp(self):
        return self.colorRampNode.color_ramp

    @property
    def colorRampNode(self):
        material = getHelperMaterial()
        node = None
        try:
            node = material.node_tree.nodes[self.colorRampid]
        except:
            pass
        if node is None:
            node = self.createRampNode()
        return node

#--------------------------------
# Corona node categories
# identifier, label, items list
#--------------------------------
corona_node_categories = [
    CoronaNodeCategory("SHADERUTILS", "Corona Maps", items = [
        NodeItem( "CoronaNormalMapNode"),
        NodeItem( "CoronaHueNode"),
        NodeItem( "CoronaMixNode"),
        NodeItem( "CoronaAONode"),
        NodeItem( "CoronaChannelNode"),
        NodeItem( "CoronaColorRampNode"),
        NodeItem( "CoronaCombineRGBNode"),
        NodeItem( "CoronaCurveNode"),
        NodeItem( "CoronaCurveNode2"),
        NodeItem( "CoronaDataNode"),
        NodeItem( "CoronaFalloffNode"),
        NodeItem( "CoronaFresnelNode"),
        NodeItem( "CoronaFrontBackNode"),
        NodeItem( "CoronaInterpolationNode"),
        NodeItem( "CoronaMathNode"),
        NodeItem( "CoronaNormalNode"),
        NodeItem( "CoronaObjectListNode"),
        NodeItem( "CoronaRaySwitchNode"),
        NodeItem( "CoronaRGBCurveNode"),
        NodeItem( "CoronaRoundEdgesNode"),
        NodeItem( "CoronaMultiNode"),
        NodeItem( "CoronaSeparateRGBNode"),
        NodeItem( "CoronaToneMapNode"),
        NodeItem( "CoronaWireframeNode")
        ]),
    CoronaNodeCategory("TEXTURE", "Corona Textures", items = [
        NodeItem( "CoronaTexNode"),
        NodeItem( "CoronaCheckerNode"),
        NodeItem( "CoronaGradientNode"),
        NodeItem( "CoronaNoiseNode"),
        NodeItem( "CoronaSolidNode"),
        NodeItem( "CoronaUVWNode")
        ]),
    CoronaNodeCategory("OUTPUT", "Corona Materials", items = [
        NodeItem( "CoronaMtlNode"),
        NodeItem( "CoronaHairMtlNode"),
        NodeItem( "CoronaSkinMtlNode"),
        NodeItem( "CoronaLayeredOutputNode"),
        NodeItem( "CoronaLightMtlNode"),
        NodeItem( "CoronaOutputNode"),
        NodeItem( "CoronaRaySwitchOutputNode"),
        NodeItem( "CoronaShadowcatcherOutputNode"),
        NodeItem( "CoronaVolumeMtlNode")
    ])]

def register():

    nodeitems_utils.register_node_categories("CORONA_NODES", corona_node_categories)

    bpy.utils.register_class( CoronaTexNode)
    bpy.utils.register_class( CoronaNormalMapNode)
    bpy.utils.register_class( CoronaAONode)
    bpy.utils.register_class( CoronaMtlNode)
    bpy.utils.register_class( CoronaLightMtlNode)
    bpy.utils.register_class( CoronaVolumeMtlNode)
    bpy.utils.register_class( CoronaMixNode)
    bpy.utils.register_class( CoronaRaySwitchNode)
    bpy.utils.register_class( CoronaSolidNode)
    bpy.utils.register_class( CoronaCheckerNode)
    bpy.utils.register_class( CoronaNoiseNode)
    bpy.utils.register_class( CoronaUVWNode)
    bpy.utils.register_class( CoronaShadowcatcherOutputNode)
    bpy.utils.register_class( CoronaHairMtlNode)
    bpy.utils.register_class( CoronaSkinMtlNode)

def unregister():
    nodeitems_utils.unregister_node_categories("CORONA_NODES")

    bpy.utils.unregister_class( CoronaTexNode)
    bpy.utils.unregister_class( CoronaNormalMapNode)
    bpy.utils.unregister_class( CoronaAONode)
    bpy.utils.unregister_class( CoronaMtlNode)
    bpy.utils.unregister_class( CoronaLightMtlNode)
    bpy.utils.unregister_class( CoronaVolumeMtlNode)
    bpy.utils.unregister_class( CoronaMixNode)
    bpy.utils.unregister_class( CoronaRaySwitchNode)
    bpy.utils.unregister_class( CoronaSolidNode)
    bpy.utils.unregister_class( CoronaCheckerNode)
    bpy.utils.unregister_class( CoronaNoiseNode)
    bpy.utils.unregister_class( CoronaUVWNode)
    bpy.utils.unregister_class( CoronaShadowcatcherOutputNode)
    bpy.utils.unregister_class( CoronaHairMtlNode)
    bpy.utils.unregister_class( CoronaSkinMtlNode)
